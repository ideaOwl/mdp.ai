var HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: {
	'dist/website/public/js/mdpai': './javascript/mdpai.js',
  'dist/website/public/js/playground': './website/public/js/playground.js',
  'dist/website/public/js/root': './website/public/js/root.js'
  },
  output: {
    path: path.resolve(__dirname),//'dist'
    filename: '[name].js'
  },
  resolve: {
	  extensions: ['.js']
  },
  devServer: {
    contentBase: "dist/website/",
    // publicPath: "/",
    port: 8090
  },
  plugins: [
    new HtmlWebpackPlugin({
      hash: true,
      title: 'Playground',
      chunks: ['dist/website/public/js/playground'], //'dist/website/public/js/mdpai', 
      template: './website/playground/index.html',
      filename: 'dist/website/playground/index.html'
    }),
    new HtmlWebpackPlugin({
      hash: true,
      title: 'Playground',
      chunks: ['dist/website/public/js/root'], //'dist/website/public/js/mdpai', 
      template: './website/index.html',
      filename: 'dist/website/index.html'
    }),

    new CopyWebpackPlugin([
      { from: 'website/public/assets', to: 'dist/website/public/assets' },
      { from: 'website/public/css', to: 'dist/website/public/css' }
    ])
  ]
};