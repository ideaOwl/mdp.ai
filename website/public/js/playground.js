import MDPAI from '../../../javascript/mdpai.js';


let curMDP = new MDPAI([

    {
        type: 'random-walk-with-actions',
        statesWOTerminal: 5,
        version: 'v1_001',
        inElement: '#mdpaiGenerator'
    },


    {
        'state-action-state':
            [
                [
                    [0, 0, 0, 1],
                    [0, 1, 0, 0]
                ],
                [
                    [0, 0, 1, 0],
                    [0.5, 0, 0, 0.5]
                ],
                [
                    [0, 0, 0, 1],
                    [0, 0, 1, 0]
                ],
                [
                    [0, 0, 1, 0],
                    [0, 0, 1, 0]
                ]
            ],
        'state-action-state-reward':
            [
                [
                    [0, 0, 0, 1],
                    [0, 0, 0, 0]
                ],
                [
                    [0, 0, 0, 0],
                    [0, 0, 0, 0]
                ],
                [
                    [0, 0, 0, 0],
                    [0, 0, 0, 0]
                ],
                [
                    [0, 0, 0, 0],
                    [0, 0, 0, 0]
                ]
            ],
        'terminal-states': ['3']
    },



    {
        type: 'srgarnet',
        states: 8,
        actions: 2,
        branchingFactor: 2,
        reward: 4,
        inElement: '#mdpaiGenerator',
        version: 'v1_001',
        _generation: {
            numInstances: 1
        }
    }

], { autoConnectPythonInterface: true });

curMDP.render();
console.log(curMDP.getData());
