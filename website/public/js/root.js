import MDPAI from '../../../javascript/mdpai.js';

document.addEventListener("DOMContentLoaded", function (e) {


    function changeViewportMeta() {
        console.log('out width', d3.select(window).node().outerWidth, d3.select('meta[name=viewport]'));
        if (d3.select(window).node().outerWidth <= 360) {
            d3.select('meta[name=viewport]').attr('content', 'initial-scale=0.34, maximum-scale=0.34, width=device-width');
        } else if (d3.select(window).node().outerWidth <= 480) {
            d3.select('meta[name=viewport]').attr('content', 'initial-scale=0.4, maximum-scale=0.4, width=device-width');
        } else if (d3.select(window).node().outerWidth <= 768) {
            d3.select('meta[name=viewport]').attr('content', 'initial-scale=0.5, maximum-scale=0.5, width=device-width');
        } else if (d3.select(window).node().outerWidth <= 1024) {
            d3.select('meta[name=viewport]').attr('content', 'initial-scale=0.6, maximum-scale=0.6, width=device-width');
        } else {
            d3.select('meta[name=viewport]').attr('content', 'initial-scale=0.9, maximum-scale=0.9, width=device-width');
        }
    }

    changeViewportMeta();


    let simpleMDPai = new MDPAI({
        type: 'state-only-equal-transitions',
        states: 6,
        version: 'v1_001',
        inElement: '#simpleMDPdiv'
    })

    simpleMDPai.render();


    let compareMDPai = new MDPAI([{
        type: 'random-walk',
        states: 6,
        version: 'v1_001',
        inElement: '#compareMDPdiv'
    }, {
        type: 'states-only',
        states: 6,
        branchingFactor: 1,
        version: 'v1_001',
        inElement: '#compareMDPdiv'
    }])

    compareMDPai.render();

    let visualization = {
        width: 300,
        height: 190
    };

    let MDPAI_v0_009 = new MDPAI([{
        type: 'srgarnet',
        states: 8,
        actions: 2,
        branchingFactor: 2,
        reward: 4,
        version: 'v1_001',
        inElement: '#carouselWithMDPs',
        visualization: visualization
    },
    {
        type: 'random-walk-with-actions',
        statesWOTerminal: 6,
        version: 'v1_001',
        inElement: '#carouselWithMDPs',
        visualization: visualization
    },
    {
        type: 'state-only-equal-transitions',
        states: 5,
        version: 'v1_001',
        inElement: '#carouselWithMDPs',
        visualization: visualization
    },
    {
        type: 'state-only-equal-transitions',
        states: 9,
        version: 'v1_001',
        inElement: '#carouselWithMDPs',
        visualization: visualization
    }, {
        type: 'srgarnet',
        states: 22,
        actions: 3,
        branchingFactor: 2,
        reward: 4,
        version: 'v1_001',
        inElement: '#carouselWithMDPs',
        visualization: visualization
    }, {
        type: 'states-only',
        states: 7,
        bMin: 1,
        bMax: 2,
        version: 'v1_002',
        inElement: '#carouselWithMDPs',
        visualization: visualization
    }
    ]);



    MDPAI_v0_009.render();




    let typed = new Typed("#carouselTypedText", {
        strings: ['Hi...'],
        typeSpeed: 20
    });

    d3.selectAll('.mdpInstance')
        .filter((d, i) => i == 2)
        .select('.svgContent')
        .attr('transform', 'translate(-125,-125) scale(1.5)')

    d3.selectAll('.mdpInstance')
        .filter((d, i) => i == 3)
        .select('.svgContent')
        .attr('transform', 'translate(-250,-250) scale(2)')

    setTimeout(function () {

        typed.destroy();
        typed = new Typed("#carouselTypedText", {
            strings: ["I generate varied MDPs for robust RL testing"],
            typeSpeed: 20
        });

        setTimeout(function () {
            d3.select('#carouselSection')
                .classed('centered', true);
        }, 2500)
        setTimeout(function () {
            d3.select('#carouselSection')
                .classed('stopTransition', true);
        }, 3000)

        setTimeout(function () {


            d3.selectAll('#carouselSectionContainer .mdpInstance')
                .transition()
                .delay((d, i) => 200 * (i + 1))
                .duration((d, i) => 800 * (i + 1))
                .style('opacity', 1)


            setTimeout(function () {
                typed.destroy();
                typed = new Typed("#carouselTypedText", {
                    strings: ["I can visualize values, rewards, and transitions"],
                    typeSpeed: 20
                });

                setTimeout(function () {

                    MDPAI_v0_009.modules.visualization.__updateMode('evaluate');
                    MDPAI_v0_009.render();
                }, 3500)

            }, 800 * 6.5)
        }, 3000)





    }, 2100)


    window.addEventListener("resize", function () {
        // alert("the orientation of the device is now " + screen.orientation.angle);
        changeViewportMeta();
        // console.log(d3.select('body').node().getBoundingClientRect().width);
    });

    let options = {
        strings: ["generate a variety of MDPs"],
        typeSpeed: 10
    }



});