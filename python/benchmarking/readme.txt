# mdp.ai Python Agents

**Note: This is very much a work-in-progress project, thank you for your patience on the lack of documentation**

This zip files includes the sample agents that will run within the Benchmark/Test section of the mdp.ai Playground: http://mdp.ai/playground

# Quickstart

You'll likely need to install some python packages via "pip install __packagename__":
* python-socketio
* eventlet
* Flask

# Running the agents

python agent_q_learning.py
python agent_random.py