# mdp.ai Python Agents

**Note: This is very much an early work-in-progress project, thank you for your patience on the lack of documentation**

This section includes the sample agents that will run within the Benchmark/Test section of the [mdp.ai Playground](http://mdp.ai/playground).

## Quickstart

Download the following zip file:
* [mdp.ai-python-agents.zip](https://bitbucket.org/ideaOwl/mdp.ai/raw/197ec9dc1c1a575f34bceeb5c777e1333142bcab/python/benchmarking/mdp.ai-python-agents.zip)

You'll likely need to install the following python packages via "pip install __packagename__" if you use pip:

* python-socketio
* eventlet
* Flask

#### Running the agents
```
python agent_q_learning.py
```

You can also run the random agent:
```
python agent_random.py
```