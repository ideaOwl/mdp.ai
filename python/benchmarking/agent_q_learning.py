# You'll need to pip install a few things, IIRC:
# - Flask
# - eventlet
# - python-socketio
# Maybe socketio as well

import mdp_ai_interfacer
import random

class QLearningAgent:

    # This is the only function that the interfacer 
    # will call (except for the constructor)
    def step(self, env, state, reward, done, info):

        print 'Step State: ' + str(state)

        experiment_start = self.num_steps == 0

        # return random.choice(env['action_space'])
        if done or experiment_start:

            if done:
                # End of episode, update Q without parameter
                self.Q[self.last_state][self.last_action] = \
                    self.Q[self.last_state][self.last_action] + \
                    self.alpha * (reward - self.Q[self.last_state][self.last_action])
            
            self.num_steps += 1

            # New episode now, set initial action based on state
            new_action = self.q_learning_action(state)

            self.last_state = state
            self.last_action = new_action

            # return just the action since this 
            # is the start of an episode
            return new_action, self.Q

        else:

            self.num_steps += 1

            # Existing epsiode
            new_action = self.q_learning_action(state)

            # Normal update of Q
            self.Q[self.last_state][self.last_action] = \
                self.Q[self.last_state][self.last_action] + \
                self.alpha * (reward + self.Q[state][new_action] - self.Q[self.last_state][self.last_action])

            self.last_state = state
            self.last_action = new_action

            return new_action, self.Q

        return '0'
        
    #################################################

    # Initialization of the class
    def __init__(self, info):
        
        # Experiment variables
        self.alpha = 0.8

        self.reset_agent(info)

    # Resetting agent
    def reset_agent(self, info):

        # Setup some variables for tracking
        self.num_steps = 0

        # Setup Q as Q[states][actions] = 0
        self.Q = {}
        self.policy = {}
        list_of_actions = range(0, info['numActions'])
        for state in range(0, info['numStates']):
            self.Q[state] = {}
            self.policy[state] = random.choice(list_of_actions)
            for action in list_of_actions:
                self.Q[state][action] = 0


    # Pick action with Q learning approach
    def q_learning_action(self, state):
        state = int(state)
        print 'q-learning'
        # print state
        # print state, self.Q[state].values()
        max_Q_val = max(self.Q[state].values())

        # Getting actions that cause the max Q value
        possible_optimum_actions = []
        for possible_action, val in self.Q[state].iteritems():
            if  val == max_Q_val:
                possible_optimum_actions.append(possible_action)

        # Random tie-breaker between multiple best actions
        self.policy[state] = random.choice(possible_optimum_actions)

        action = self.policy[state]  # Pick based on policy

        print 'Action taken: ' + str(action)
        return action
        

mdp_ai_interfacer.connect_agent(QLearningAgent)
mdp_ai_interfacer.start()