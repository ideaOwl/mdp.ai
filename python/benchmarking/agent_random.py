# You'll need to pip install a few things, IIRC:
# - Flask
# - eventlet
# - python-socketio
# Maybe socketio as well

import mdp_ai_interfacer
import random

class RandomAgent:

    # Should return action, any added metadata you want to 
    # send to the web page
    def step(self, env, state, reward, done, info):
        action = random.choice(env['action_space'])
        print env['mdp_id'] + '|   State: ' + str(state) + '   Action: ' + action + '    Reward: ' + str(reward) + '    Done: ' + str(done)
        return action, None

    # The incoming info for the constructor should include 
    # things like the number of states        
    def __init__(self, info):
        print info

mdp_ai_interfacer.connect_agent(RandomAgent)
mdp_ai_interfacer.start()