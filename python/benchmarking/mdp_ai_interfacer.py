import socketio
import eventlet
import json
from flask import Flask, render_template

sio = socketio.Server()
app = Flask(__name__)

UserAgent = None
agentsByRoomID = {}

def update_rooms(sid, roomsData):
    
    global agentsByRoomID
    
    # print roomData
    roomIDs = map(lambda d: d['mdpInstanceID'], roomsData)

    for roomData in roomsData:
        roomID = roomData['mdpInstanceID']
        roomInfo = roomData['info']
        
        # Room/MDP exists, so reset it
        if roomID in agentsByRoomID:
            print('room being reset: ', roomID)
            del agentsByRoomID[roomID]
            agentsByRoomID[roomID] = UserAgent(roomInfo)
        # It doesn't exist, so create it
        else:
            print('room being created: ', roomID)
            sio.enter_room(sid, roomID)
        
            agentsByRoomID[roomID] = UserAgent(roomInfo)
            print "Instantiated Agent: ", agentsByRoomID[roomID]
        
            sio.emit('roomCreated', roomID)
          
    for existingRoomID in agentsByRoomID.keys():
        if existingRoomID not in roomIDs:
            print('Room no longer exists:', existingRoomID)
            del agentsByRoomID[existingRoomID]


@sio.on('connect')
def connect(sid, environ):
    print('connect ', sid)
	
#@sio.on('my message')
#def message(sid, data):
#    print('message ', data)

@sio.on('disconnect')
def disconnect(sid):
    print('disconnect ', sid)



@sio.on('resetRooms')
def reset_rooms(sid, roomIDs):
    update_rooms(sid, roomIDs)

            
@sio.on('syncRooms')
def sync_rooms(sid, roomIDs):
    update_rooms(sid, roomIDs)


def connect_agent(AgentClass):
    global UserAgent
    UserAgent = AgentClass

def start():
    global app
    # wrap Flask application with socketio's middleware
    app = socketio.Middleware(sio, app)

    # deploy as an eventlet WSGI server
    eventlet.wsgi.server(eventlet.listen(('', 8067)), app)


@sio.on('requestAgentStepAction')
def request_agent_step_action(sid, data):
    #print data
    agent_action = agentsByRoomID[data['roomID']].step(data['env'], data['state'], data['reward'], data['done'], data['info'])
    sio.emit('actionTaken', {'action': agent_action[0], 'roomID': data['roomID'], 'data': agent_action[1]}, room=data['roomID'])
    
    
    


    

if __name__ == '__main__':
    print "mdp_ai_interfacer is meant to be imported and have an agent attached.  See http://mdp.ai/needToFillThisInLater"
    pass