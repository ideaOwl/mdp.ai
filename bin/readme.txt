// Rollup
rollup -c

// Install IF NOT DONE ALREADY (this is a one-time thing)
yarn global add uglify-es

// Uglifying
uglifyjs mdpai.js --output mdpai.min.v0.1.0.js -c -m