'use strict'

import MDPAI_Generator_SRGARNET_v1 from './generators/mdpai_generator_srgarnet_v1.js';
import MDPAI_Generator_EQUAL_STATES_ONLY_v1 from './generators/mdpai_generator_equal_states_only_v1.js';
import MDPAI_Generator_STATES_ONLY_v1 from './generators/mdpai_generator_states_only_v1.js';
import MDPAI_Generator_STATES_ONLY_v1_002 from './generators/mdpai_generator_states_only_v1_002.js';
import MDPAI_Generator_RANDOM_WALK_v1_001 from './generators/mdpai_generator_random_walk_v1.js';
import MDPAI_Generator_RANDOM_WALK_WITH_ACTIONS_v1 from './generators/mdpai_generator_random_walk_with_actions_v1.js';
import MDPAI_Generator_RANDOM_MDP_v1 from './generators/mdpai_generator_random_mdp_v1.js';
import MDPAI_Generator_USER_PROVIDED_MATRICES_v1 from './generators/mdpai_generator_user_provided_matrices.js';


class MDPAI_Generation{

    constructor(core, params) {
        let self = this;
        self.__setupSelfVariables(core, params);

		self.generate(params);
    }

    __setupSelfVariables(core, params) {
        let self = this;
        self._core = core;

        self.instantiatedGenerators = [];

		self._activeGenerator = null;

		self._generators = self.__registerGenerators([
            MDPAI_Generator_RANDOM_MDP_v1,
            MDPAI_Generator_RANDOM_WALK_v1_001,
            MDPAI_Generator_RANDOM_WALK_WITH_ACTIONS_v1,
            MDPAI_Generator_SRGARNET_v1,
            MDPAI_Generator_STATES_ONLY_v1,
            MDPAI_Generator_STATES_ONLY_v1_002,
			MDPAI_Generator_EQUAL_STATES_ONLY_v1,
			MDPAI_Generator_USER_PROVIDED_MATRICES_v1
		]);
    }

	__registerGenerators(generatorClasses) {
        let self = this;
		let generators = {};
		for (let generatorClass of generatorClasses) {
			let [info, generatorID] = self.__uniqueGeneratorID(generatorClass.generatorInfo(), generators);

			generators[generatorID] = {
				info: info,
				generator: generatorClass
			}
		}
		return generators;
	}

	__uniqueGeneratorID(info, generators) {
        let self = this;
		let generatorID = info.type + '___' + info.version;
		if (generators[generatorID] !== undefined) {
			info.version += '_x';
			return self.__uniqueGeneratorID(info, generators);
		} else {
			info.id = generatorID;
			return [info, generatorID];
		}
	}

	list() {
        let self = this;
		return Object.keys(self._generators);
	}

    getSpecifiedGenerator(params) {
		let self = this;
		let selectedGenerator = params.type + '___' + (params.version || 'v1');

		// If generator not found, assume it's the user provided matrices.
		if (self._generators[selectedGenerator] === undefined) {
			selectedGenerator = 'user-provided-matrices' + '___' + 'v1_001';
		}
		
		params.seed = params.seed || Math.random();
        return self._generators[selectedGenerator]
    }

	generate(params) {
        let self = this;
		self._activeGenerator = self.getSpecifiedGenerator(params);

        let numInstances = 1;
        if(params._generation && params._generation.numInstances) {
            numInstances = params._generation.numInstances;
        }

        for (let i = 0; i < numInstances; i++) {
            new self._activeGenerator.generator(
    			self._core, params
    		);
        }
	}



}


export default MDPAI_Generation;
