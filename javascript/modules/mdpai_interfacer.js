'use strict'
// // import ioClient from 'socket.io-client';  // Swap with above for webpack.
// import * as ioClient from './_dependencies/socket.io.2.1.0';//'socket.io-client';  // Use this if dealing with rollup

class MDP_AI_Interfacer{

    constructor(core) {
        let self = this;
        self.__setupSelfVariables(core);
        if (core._settings.autoConnectPythonInterface) {
            self.seekServer();
        }
    }

    __setupSelfVariables(core) {
        let self = this;
        self._core = core;
        self._timers = {
            seekServer: null
        };
        self._s = {
            connectedToServer: false,
            server: null,
            mdpsAwaitingActions: {},
            // runStepQueue: 0
        };
        self._settings =  {
            server: {
                port: 8067
            }
        }
    }

    runStep(numSteps) {
        let self = this;

        if (numSteps === undefined) {numSteps = 1};

        if (numSteps !== 0) {

            let queueExists = JSON.stringify(self._s.mdpsAwaitingActions) !== '{}';

            let shouldLetActionTakenRunNextSteps = false;

            // if () {
            for (let mdpData of self._core._data.mdps) {
                if (!mdpData.generationMatrixes._hasActions) continue;
                if (self._s.mdpsAwaitingActions[mdpData.instanceID] === undefined) {
                    self._s.mdpsAwaitingActions[mdpData.instanceID] = 0;
                }

                self._s.mdpsAwaitingActions[mdpData.instanceID] += numSteps;

                if (self._s.mdpsAwaitingActions[mdpData.instanceID] !== 1) {
                    shouldLetActionTakenRunNextSteps = true;
                }
                // if () {

                // }
                
            }

            if (queueExists && shouldLetActionTakenRunNextSteps) return;

            // self._s.runStepQueue += 1;
            // }
        }

        let ioServer = self._s.server;


        for(let mdpData of self._core._data.mdps) {
            if (!mdpData.generationMatrixes._hasActions) continue;
            let reward = 0;
            let done = false;
            let meta = mdpData.generationMatrixes['_meta'];

            let agentInState = meta.filter(d=>d.agent);
            if (agentInState.length === 0) {
                agentInState = meta.filter(d=>d.start)[0];
                agentInState['agent'] = true;
            } else {
                agentInState = agentInState[0];
            }

            if (mdpData.generationMatrixes.interfacerVars === undefined) {
                mdpData.generationMatrixes.interfacerVars = {
                    lastAction: null,
                    reward: null,
                    numStepsSoFar: 1,
                    totalReturn: 0,
                    numStates: mdpData.generationMatrixes['state-action-state'].length,
                    numActions: mdpData.generationMatrixes['state-action-state'][0].length
                }
            } else {
                reward = mdpData.generationMatrixes.interfacerVars.reward;
                done = mdpData.generationMatrixes.interfacerVars.done;
            }


            ioServer.emit('requestAgentStepAction', {
                roomID: mdpData.instanceID,
                env: {
                    action_space: agentInState.actions.map(d=>d.actionID),
                    mdp_id: mdpData.instanceID
                },
                state: +agentInState.stateID,
                reward: reward,
                done: done,
                info: { 
                    interface: mdpData.generationMatrixes.interfacerVars,
                 }
            });
        }

    }


    resetExperiment() {
        let self = this;
        // Remove all interfacer vars, forcing a reset
        self._core._data.mdps.map(mdpData => {
            mdpData.generationMatrixes.interfacerVars = undefined
            mdpData.generationMatrixes._meta.map(meta => {
                delete meta.agent;
                if (meta.start) meta.agent = true;
            })
        });
        // Reset agents within meta to their original state

        self.__resetMDPsToServer();
        self._core.modules.visualization.render();
    }

    __initVarsForAgents() {
        let self = this;
        return self._core._data.mdps.filter(mdpData => {
                return mdpData.generationMatrixes._hasActions
            }).map(mdpData => {
            // return mdpData.instanceID

            if (mdpData.generationMatrixes.interfacerVars === undefined) {
                mdpData.generationMatrixes.interfacerVars = {
                    lastAction: null,
                    reward: null,
                    numStepsSoFar: 0,
                    totalReturn: 0,
                    done: false,
                    returnHistory: [],
                    numStates: mdpData.generator._params.states || mdpData.generationMatrixes['state-action-state'].length,
                    numActions: mdpData.generator._params.actions || mdpData.generationMatrixes['state-action-state'][0].length
                }
            }

            return {
                mdpInstanceID: mdpData.instanceID,
                info: mdpData.generationMatrixes.interfacerVars
            }
        })
    }

    __syncMDPsToServer() {
        let self = this;
        let ioServer = self._s.server;
        ioServer.emit('syncRooms', self.__initVarsForAgents());
    }

    __resetMDPsToServer() {
        let self = this;
        let ioServer = self._s.server;
        ioServer.emit('resetRooms', self.__initVarsForAgents());
    }

    seekServer() {
        let self = this;

        let ioServer = self._s.server;

        // ioServer = ioClient('http://localhost:' + self._settings.server.port +'/'); //'http://localhost:' + self._settings.server.port
        ioServer = io('http://localhost:' + self._settings.server.port + '/'); 
        ioServer.on('connect', function(){
            self._s.connectedToServer = true;
            console.log('=======connected');

            ioServer.emit('my message', 'world');
            // console.log(self._core._data.mdps);

            self._core.modules.visualization.render();

            self.__syncMDPsToServer();
        });
        ioServer.on('event', function(data){
            // console.log('event', data);
        });
        ioServer.on('roomCreated', d =>  {
            // console.log('mdp created from server: ', d);
        });

        ioServer.on('actionTaken', function(data){

            let episodeDone = false;
            data.action = String(data.action);
            let mdpsData = self._core._data.mdps;
            let mdpData = mdpsData.filter(d=>d.instanceID === data.roomID)[0];
            let meta = mdpData.generationMatrixes._meta;
            let interfaceVars = mdpData.generationMatrixes.interfacerVars;

            if (data.data !== null) {

                if (interfaceVars['_fromAgent'] === undefined) {
                    interfaceVars['_fromAgent'] = {}
                    interfaceVars['_fromAgent']['Q'] = data.data;
                }
    
                interfaceVars['_fromAgent']['Q'] = data.data;
    
                let maxQ = interfaceVars['_fromAgent']['maxQValue'] || 0;
                for (let [state, actionValuePair] of Object.entries(interfaceVars['_fromAgent']['Q'])) {
                    for (let [action, Qvalue] of Object.entries(actionValuePair)) {
                        if (Qvalue >= maxQ) {
                            interfaceVars['_fromAgent']['maxQValue'] = Qvalue;
                            maxQ = Qvalue;
                        }
                    }
                }
                
            }

            interfaceVars.lastAction = data.action;
            let lastStateMeta = meta.filter(d=>d.agent)[0];
            interfaceVars.lastState = lastStateMeta.stateID;
            let actionMeta = lastStateMeta.actions.filter(d=>d.actionID === data.action)[0];

            let randomTransition = Math.random();
            let transitionScore = 0;

            function shuffleArray(array) {
                for (let i = array.length - 1; i > 0; i--) {
                    let j = Math.floor(Math.random() * (i + 1));
                    [array[i], array[j]] = [array[j], array[i]];
                }
            }

            shuffleArray(actionMeta.to);

            for (let toStateMeta of actionMeta.to) {
                transitionScore += toStateMeta.prob;
                if(randomTransition < transitionScore) {
                    interfaceVars.newState = toStateMeta.toStateID;
                    interfaceVars.reward = toStateMeta.reward;
                    interfaceVars.totalReturn += toStateMeta.reward;
                    interfaceVars.numStepsSoFar += 1;
                    interfaceVars.returnHistory.push(interfaceVars.totalReturn);
                    break;
                }
            }

            if(interfaceVars.lastState !== interfaceVars.newState) {
                meta.filter(d=>{
                    if(interfaceVars.lastState === d.stateID) {
                        delete d.agent;
                    }
                    if(interfaceVars.newState === d.stateID) {
                        if (!d.terminal) {
                            d.agent = true;
                        } else {
                            meta.filter(a=>a.start)[0].agent = true;
                            episodeDone = true;
                        }
                    }
                })
            }

            interfaceVars.done = episodeDone;


            // console.log('instance data returned', data.roomID)
            
            // console.log(self._s.mdpsAwaitingActions[data.roomID]);

            self._s.mdpsAwaitingActions[data.roomID] -= 1;

            // console.log('Entries', Object.entries(self._s.mdpsAwaitingActions));
            let currNumStepsLeft = undefined;
            let runNextStep = true;
            for (let [instanceID, numStepsLeft] of Object.entries(self._s.mdpsAwaitingActions)) {
                // console.log(instanceID, numStepsLeft, currNumStepsLeft, numStepsLeft);
                if (currNumStepsLeft === undefined) {
                    currNumStepsLeft = numStepsLeft;
                }

                if (currNumStepsLeft !== numStepsLeft) {
                    runNextStep = false;
                    return;
                }
                // self._s.mdpsAwaitingActions[instanceID] -= 1;
                // console.log(self._s.mdpsAwaitingActions[instanceID]);
                // if (self._s.mdpsAwaitingActions[instanceID] <= 0) {
                //     delete self._s.mdpsAwaitingActions[instanceID]
                // }
            }

            if (Object.values(self._s.mdpsAwaitingActions).reduce((a,b)=> a + b, 0) === 0) {
                self._s.mdpsAwaitingActions = {};
            }

            // // console.log('========== interface vars', interfaceVars);
            // for (let [instanceID, numStepsLeft] of Object.entries(self._s.mdpsAwaitingActions)) {
            //     self._s.mdpsAwaitingActions[instanceID] -= 1;
            //     console.log(self._s.mdpsAwaitingActions[instanceID]);
            //     if (self._s.mdpsAwaitingActions[instanceID] <= 0) {
            //         delete self._s.mdpsAwaitingActions[instanceID]
            //     }
            // }

            if (JSON.stringify(self._s.mdpsAwaitingActions) === '{}') {
                // console.log('=======no awaiting actions')
                self._core.modules.visualization.render();
            } else {
                // console.log('=======attempt run step')
                self.runStep(0);
            }
        });

        ioServer.on('disconnect', function(){
            self._s.connectedToServer = false;
            // console.log('-------disconnect');
            self._core.modules.visualization.render();
        });

        ioServer.on('reconnect_attempt', () => {
            if(self._core.modules.visualization._state.mode !== 'evaluate') {
                return;
            }
            self._s.connectedToServer = false;
            // console.log('???????attempt disconnect');
        });


        self._s.server = ioServer;


        // if(!self._s.connectedToServer
        //     && self._timers.seekServer === null) {
        //
        //     self._timers.seekServer = setTimeInterval(
        //         function() {
        //
        //         }, self._s.timeToCheckServer
        //     );
        //
        // }
    }




}


export default MDP_AI_Interfacer;
