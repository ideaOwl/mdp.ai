'use strict'

class MDPAI_Solver{

    constructor(core) {
        let self = this;
        self.__setupSelfVariables(core);
    }

    __setupSelfVariables(core) {
        let self = this;
        self._core = core;
    }


    updateValueForState({state, v, pi, meta}) {
        let self = this;
        let stateMeta = meta[state];
        let actionValues, maxAction, maxValue, transitionValues;
        if (stateMeta.actions) {
            actionValues = stateMeta.actions.map(d => d.to.reduce((carry, to) => carry + +(to.prob * (+to.reward + 0.56*v[+to.toStateID])) , 0))
            maxValue = Math.max(...actionValues);
            let actionsWithMaxValue = [];
            actionValues.map((d,i) => {if(maxValue === d) {actionsWithMaxValue.push(i)}});
            maxAction = actionsWithMaxValue[Math.floor(Math.random() * actionsWithMaxValue.length)];
        } else {
            transitionValues = stateMeta.to.map(to => +(to.prob * (+to.reward + 0.56*v[+to.toStateID])) , 0)
            maxValue = Math.max(...transitionValues);
            let actionsWithMaxValue = [];
            transitionValues.map((d,i) => {if(maxValue === d) {actionsWithMaxValue.push(i)}});
            maxAction = actionsWithMaxValue[Math.floor(Math.random() * actionsWithMaxValue.length)];
        }

        return { _v: maxValue, _pi: maxAction, _hasActions: stateMeta.actions !== undefined}
    }

    solve(p, meta) {
        let self = this;
        // self._s.pi = {};
        // self._s.v = {};

        // let meta = self._core._data.generationMatrixes[0]._meta;
        // console.log('attempt solving');
        // debugger;

        let states = p.states || p['state-action-state'].length;

        var pi = Array.from(Array(states), () => -1);
        var v = Array.from(Array(states), () => 0); //Array.from(Array(states).keys());
        var hasActions = false;

        let theta = p.theta || 0.01;
        let delta = theta;
        let count = 0;
        while (true) {
            delta = 0;
            count++;
            for (let state = 0; state < states; state++) {
                let oldV = v[state]+0;
                let { _v, _pi, _hasActions} = self.updateValueForState({state: state, v: v, pi: pi, meta: meta});
                delta = Math.max(...[delta+0, Math.abs(oldV - _v)]);
                v[state] = _v;
                pi[state] = _pi;
                hasActions = _hasActions
            }
            if (count % 200 === 0) {
                // console.log(delta);
                if (count > 1000000) {
                    break;
                }
                // debugger;
            }
            if (delta < theta) {
                // console.log('k', count);
                break;
            }
        }

        return {v:v, pi:pi, hasActions: hasActions};
    }

}


export default MDPAI_Solver;
