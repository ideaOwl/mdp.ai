'use strict'
import d3 from './_dependencies/d3roll.min.4.13.0';
// import * as d3 from 'd3';

class MDPAI_Visualization {
    constructor(core, properties) {
        let self = this;
        self.__setupSelfVariables(core, properties);
        self.generateViz(core, properties);
    }

    __setupSelfVariables(core, properties) {
        let self = this;
        self._core = core;
        self._settings = properties || {};
        self._state = {
            id: null,
            mode: 'generate',
            language: 'python',
            transitionDuration: 250,
            freezeSimulation: false
        }
        self._objs = {
            container: null,
            params: {}
        };

        if (core._settings.originalProperties.visualization === undefined) {
            core._settings.originalProperties.visualization = {};
        }

        core._settings.originalProperties.visualization.width = core._settings.originalProperties.visualization.width;
        core._settings.originalProperties.visualization.height = core._settings.originalProperties.visualization.height;

        self._generationParameters = [
            {name: 'fixNodePositions', label: 'Fix Node Positions', type: Boolean, default: false},
            {name: 'trasitionLines', label: 'Enable Transition Lines', type: Boolean, default: false},
            {name: 'setMode', label: 'Mode', type: String, controlType: 'radio', options: [
                {name:'basic', label: 'Basic'},
                {name:'planning', label: 'Planning', default: true},
                {name:'experiment', label: 'Experiment', disabled: true}
            ]}
        ];

    }

    __generateRandomID(length) {
        return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
    }

    __generateInstanceID() {
        let self = this;
        let idUnique = false;
        let currID = "";
        while(!idUnique) {
            currID = 'MDPAI_' + self.__generateRandomID(5);
            idUnique = d3.selectAll('.'+currID).empty();
        }
        return currID;
    }

    __generateHtmlElementsControls(id, container) {
        let self = this;

        let sandboxContents = container.append('div')
            .classed('sandboxContents', true);

        sandboxContents.append('div')
            .classed('mdpTooltip', true);

        let controls = sandboxContents.append('div')
            .classed('mdpai_controls', true);

        let tabSection = controls
            .append('div')
            .classed('tabSection', true)
            .selectAll('tab')
            .data([
                {'label': 'Generate', 'id': 'generate'},
                {'label': 'Benchmark/Test', 'id': 'benchmark'}
            ]);

        tabSection.enter()
            .append('div')
            .classed('tab', true)
            .classed('selected', (d,i) => i === 0)
            .classed('rightSide', (d,i) => d.id === 'faq')
            .text(d=>d.label)
            .on('click', function(d) {
                self.__updateMode(d.id.toLowerCase());
            });
        

        let generateSection = controls.append('div')
            .classed('generateSection', true)
            .classed('menuSection', true);

        let evaluateSection = controls.append('div')
            .classed('evaluateSection', true)
            .classed('menuSection', true);
        
        let legendSection = controls.append('div')
            .classed('legendSection', true)
            .classed('menuSection', true)
            .classed('hidden', true)
            .on('click', function(){
                self.__applyToAllAction('map');
            })
            .append('img')
            .attr('src','../public/assets/visualization-legend.png') ;

        let generatorsControls = generateSection.append('div')
            .classed('generatorsControls', true)
            .classed('menuSubsection', true);

        generatorsControls.append('div').text('Generator').classed('generatorTitle', true);

        let selectGenerator = generatorsControls.append('select')
            .classed(id+'_select_generators', true)
            .attr('name', id+'_select_generators')
            .on('change', d=> {
                self.__generateGeneratorControls(id, generateSection);
            });

        self._objs.params.type = selectGenerator;

        let generatorOptions = selectGenerator.selectAll('option')
            .data(self._core.generators.list().map(d => self._core.generators._generators[d].info))
            .enter()
            .append('option')
            .attr('value', d => d.id)
            .property('selected',
                d => d.id === self._core.generators._activeGenerator.info.id) // Default
            .text(d => (d.name ? d.name : d.type) + ' (' + d.version + ')');

        self.__generateGeneratorControls(id, generateSection);

        let addMDPControls = generateSection.append('div')
            .classed('addMDPsControls', true)
            .classed('menuSubsection', true);
            
        addMDPControls.append('input')
            .attr('type', 'button')
            .classed(id+'_button_generate', true)
            .attr('value', 'Add')
            .on('click', function() {
                self.__generateAndRenderMDPs()
            });

        addMDPControls.append('select')
            .classed(id+'_select_num_generators', true)
            .classed('numGeneratorsSelect', true)
            .selectAll('option')
            .data(Array.from(Array(20).keys()).map(d=>d+1))
            .enter()
            .append('option')
            .attr('value', d=>d)
            .property('selected', d => d === 2) // Default
            .text(d=>d);

        addMDPControls.append('div')
            .style('margin-top', '4px')
            .style('font-size', '13px')
            .text('mdp instances');


        let setControls = container.append('div')
            .classed(id + '_set_controls', true)
            .classed('set_controls', true);

        setControls.append('input')
            .attr('type', 'file')
            .attr('multiple', true)
            .attr('name', 'loadFileInput')
            .classed('loadFileInput', true)
            .attr('id', self._state.id + '_loadFileInput')
            .style('width', '0px')
            .style('height', '0px')
            .style('opacity', '0');

        let controlIconLabelsEntered = setControls.selectAll('i')
            .data(['refresh', 'show_chart', 'palette', 'map', 'save', 'folder_open', 'info', 'delete']) // 'palette', 
            .enter()
            .append('label')
            .attr('for', function(e){
                if (e === 'folder_open') {
                    return self._state.id + '_loadFileInput';
                }
                return null;
            })
            .attr('actionType', e => e)
            .append('div')
            .classed('applyAllIconsDiv', true)
            .on('click', e => self.__applyToAllAction(e))
            .on('dblclick', e => self.__applyToAllDoubleClickAction(e))
            .on('mouseover', function (e) {
                let message = '';
                if (e === 'delete') {
                    message = 'Please Double Click to Delete All';
                    self.__updateTooltipMessages(message);
                    self.__updateTooltip(d3.event);
                };
                
                // if (e === 'refresh') message = 'Regenerate All MDPs';
                // if (e === 'show_chart') message = 'Toggle All Charts';
                // if (e === 'palette') message = 'Visualize Values for All';
                // if (e === 'save') message = 'Save All MDPs as Single JSON Set';
                // if (e === 'info') message = 'Show Parameters for all MDPs';
                // if (e === 'delete') message = 'Delete All (Double Click)';
                // if (e === 'folder_open') message = 'Load MDP (You can also drag & drop the file in)';
                // self.__updateTooltipMessages(message);
                // self.__updateTooltip(d3.event);
            })
            .on('mousemove', function (e) {
                if (e === 'delete') {
                    self.__updateTooltip(d3.event);
                };
            })
            .on('mouseout', d => {
                self._objs.container.select('.mdpTooltip').classed('show', false);
            })

        controlIconLabelsEntered
            .append('i')
            .attr('class', e => 'icon_' + e)
            .classed('material-icons', true)
            .classed('applyToAllIcon', true)
            .text(e => e)

        controlIconLabelsEntered.append('div')
        .classed('iconLabel', true)
        .text(function(e){
            let message = '';
            if (e === 'refresh') message = 'Regen';
            if (e === 'show_chart') message = 'Charts';
            if (e === 'palette') message = 'Values';
            if (e === 'map') message = 'Legend';
            if (e === 'save') message = 'Save';
            if (e === 'info') message = 'Vars';
            if (e === 'delete') message = 'Delete';
            if (e === 'folder_open') message = 'Load';
            return message;
        })


        document.getElementById(self._state.id + '_loadFileInput')
            .addEventListener('change', function (evt) {
                var files = evt.target.files;
                if (files.length > 0) {
                    self.__loadFiles(files);
                }
            }, false)

        // setControls
        //     .append('input')
        //     .attr('type', 'button')
        //     .classed(id + '_button_generate', true)
        //     .classed('button_generate', true)
        //     .attr('value', 'Regenerate All')
        //     // .style('display', 'none')
        //     .on('click', function() {

        //         // self._state.transitionDuration = 0;
        //         // self._state.freezeSimulation = true;


        //         self.__regenerateMDPsWithExistingGenerators();
        //     });

            
        // let visualizeControls = setControls
        //     .append('label')
        //     .on('click', function() {
        //         let visualizeValues = d3.select(this).select('input').node().checked;
        //         self.__updateMode(visualizeValues ? 'evaluate' : 'generate');
        //     });

        // visualizeControls
        //     .append('input')
        //     .classed(id+'_visualizeCheckbox', true)
        //     .attr('type', 'checkbox')

        // visualizeControls
        //     .append('span')
        //     .text('Visualize values')

        // let fixedControls = setControls
        //     .append('label')
        //     .on('click', function() {

        //         if (setControls.select('.'+id+'_fixedCheckbox').property('checked')) {
        //             self._core._data.mdps.filter(mdpData=> {
        //                 mdpData.vizVars.mergedNodes.filter(node=> {
        //                     if(mdpData.vizVars.fixedNodePositions === undefined) {
        //                         mdpData.vizVars.fixedNodePositions = {};
        //                     }
        //                     mdpData.vizVars.fixedNodePositions[node.nodeID] = {
        //                         fx: node.x, fy: node.y
        //                     }
        //                 })
        //             })
        //         } else {
        //             self._core._data.mdps.filter(mdpData=> {
        //                 mdpData.vizVars.fixedNodePositions = undefined
        //                 // mdpData.vizVars.mergedNodes.filter(node=> {
        //                 //     mdpData.vizVars.fixedNodePositions = undefined;
        //                 //     // if(mdpData.vizVars.fixedNodePositions === undefined) {
        //                 //     //     mdpData.vizVars.fixedNodePositions = {};
        //                 //     // }
        //                 //     // mdpData.vizVars.fixedNodePositions[node.nodeID] = {
        //                 //     //     fx: node.x, fy: node.y
        //                 //     // }
        //                 // })
        //             })
        //         }
        //     });

        // fixedControls
        //     .append('input')
        //     .classed(id+'_fixedCheckbox', true)
        //     .attr('type', 'checkbox')

        // fixedControls
        //     .append('span')
        //     .text('Pause Gravity')


        // setControls
        //     .append('input')
        //     .attr('type', 'button')
        //     .classed(id+'_button_save_set', true)
        //     .classed('button_save_set', true)
        //     .attr('value', 'Save Set')
        //     // .style('display', 'none')
        //     .on('click', function() {
        //         self.saveMDPsToFile();
        //     });

        let zoomControls = setControls
            .append('div')
            .classed('zoomControls', true);

        zoomControls.selectAll('zoomIcons')
            .data(['zoom_in','zoom_out'])
            .enter()
            .append('i')
            .attr('class', e => 'icon_' + e)
            .classed('material-icons', true)
            .text(d=>d)
            .on('click', function (d) {
                self.changeZoom(d==='zoom_in' ? 1 : -1, container);
            })
            .on('mouseover', function (e) {
                let message = '';
                if (e === 'zoom_in') message = 'Zoom In';
                if (e === 'zoom_out') message = 'Zoom Out';
                self.__updateTooltipMessages(message);
                self.__updateTooltip(d3.event);
            })
            .on('mousemove', function () {
                self.__updateTooltip(d3.event);
            })
            .on('mouseout', d => {
                self._objs.container.select('.mdpTooltip').classed('show', false);
            })


        // let modeControls = setControls
        //     .append('div')
        //     .classed(id+'_modeControls', true)

        // let modeOptions = modeControls
        //     .selectAll('.modeOption')
        //     .data(['Generate', 'Evaluate', 'Benchmark'], d=>d)
        //     .enter()
        //     // .append('div')
        //     .append('label')
        //     .classed('modeOption', true)
        //     .attr('for', d=>id+'_mode_'+d)
        //     .on('click', d => {
        //         self.__updateMode(d.toLowerCase());
        //     });

        // modeOptions
        //     .append('input')
        //     .attr('type', 'radio')
        //     .attr('name', id+'_mode')
        //     .attr('id', d=>id+'_mode_'+d)
        //     .attr('value', d => d.toLowerCase())
        //     .property('checked', d=> d.toLowerCase() === self._state.mode)

        // modeOptions
        //     .append('span')
        //     .text(d => d);


        // // let modeControls = setControls
        // //     .append('div')
        // //     .classed(id+'_modeControls', true)
        // //
        // // let modeOptions = modeControls
        // //     .selectAll('.modeOption')
        // //     .data(['Generate', 'Evaluate', 'Benchmark'], d=>d)
        // //     .enter()
        // //     // .append('div')
        // //     .append('label')
        // //     .classed('modeOption', true)
        // //     .attr('for', d=>id+'_mode_'+d)
        // //     .on('click', d => {
        // //         self.__updateMode(d.toLowerCase());
        // //     });
        // //
        // // modeOptions
        // //     .append('input')
        // //     .attr('type', 'radio')
        // //     .attr('name', id+'_mode')
        // //     .attr('id', d=>id+'_mode_'+d)
        // //     .attr('value', d => d.toLowerCase())
        // //     .property('checked', d=> d.toLowerCase() === self._state.mode)
        // //
        // // modeOptions
        // //     .append('span')
        // //     .text(d => d);



        let benchmarkControls = evaluateSection
            .append('div')
            .classed(id+'_benchmarkControls', true)
            .classed('benchmarkControl', true);



        let languageOptions = benchmarkControls
        .selectAll('.languageOption')
        .data(['Python', 'Javascript'], d=>d)
        .enter()
        // .append('div')
        .append('label')
        .classed('languageOption', true)
        .attr('for', d=>id+'_language_'+d)

        .on('click', d => {
            // self.__updateLanguage(d.toLowerCase());
        });

        languageOptions
        .append('input')
        .attr('type', 'radio')
        .attr('name', id+'_language')
        .attr('id', d=>id+'_language_'+d)
        .attr('value', d => d.toLowerCase())
        .property('checked', d=> d.toLowerCase() === self._state.language)
        .attr('disabled', d => d === 'Javascript' ? true : null)

        languageOptions
        .append('span')
        .text(d => d);


        benchmarkControls.append('br');

        let sharedLanguageControls = benchmarkControls
            .append('div')
            .classed('sharedLanguageControls', true);


        
        sharedLanguageControls
            .append('input')
            .attr('type', 'button')
            .attr('value', 'Run Step')
            .on('click', () => {
                // return;
                self._core.modules.interfacer.runStep(1);
            })
        sharedLanguageControls
            .append('input')
            .attr('type', 'button')
            .attr('value', 'Run 20 Steps')
            // .attr('disabled', true)
            .on('click', () => {
                self._core.modules.interfacer.runStep(20);
            })


        sharedLanguageControls
            .append('input')
            .attr('type', 'button')
            .attr('value', 'Run 50 Steps')
            // .attr('disabled', true)
            .on('click', () => {
                self._core.modules.interfacer.runStep(50);
            })

        sharedLanguageControls
            .append('input')
            .attr('type', 'button')
            .attr('value', 'Reset Experiment')
            .on('click', () => {
                self._core.modules.interfacer.resetExperiment();
            })


        let pythonInterfaceControls = benchmarkControls
            .append('div')
            .classed('pythonInterfaceControls', true)
            .classed('connectionStatusDisconnected', true);

        pythonInterfaceControls
            .append('div')
            .classed('pythonConnectionStatus', true)
            .html('<b>Disconnected</b>');
            

        let moreOptionsButton = setControls
            .append('div')
            .classed(id+'_button_moreOptions', true)
            .text('More')
            .style('display', 'none')

        let fixedNodesOptions = setControls
            .append('div')
            .classed(id+'_set_options_fixed_node', true)
            .style('display', 'none')

        // fixedNodesOptions
        //     .append('input')
        //     .

    }

    changeZoom(direction, container) {
        let self = this;
        let hasZoomPreference = !container.select('.mdpsContainer[zoomlevel]').empty();
        let zoomPreference = 2;
        if (hasZoomPreference) {
            zoomPreference = +container.select('.mdpsContainer').attr('zoomlevel');
        }
        let newZoomPreference = zoomPreference + +direction;
        if (newZoomPreference < 1) newZoomPreference = 1;
        if (newZoomPreference > 4) newZoomPreference = 4;
        container.select('.mdpsContainer')
            .attr('zoomlevel', newZoomPreference);
    }

    __generateGeneratorControls(id, generateSection) {
        let self = this;

        // reset params

        let params = self._core
            .generators._generators[self._objs.params.type.property('value')].info.parameters;

        generateSection.selectAll('.controlSection').remove();

        let paramsSubsection = generateSection.select('.paramsSubsection');
        if (paramsSubsection.empty()) {
            paramsSubsection = generateSection.append('div')
                .classed('paramsSubsection', true)
                .classed('menuSubsection', true);
        }

        for (let param of params) {
            // console.log(param);



            let controlObj = paramsSubsection.select('.'+id+'_select_'+param.name)
            if(controlObj.empty()) {
                controlObj = paramsSubsection.append('div')
                    .classed('controlSection', true);


                let paramTypeName = 'select';

                if (param.type === 'textarea') {
                    paramTypeName = 'textarea'

                    controlObj.append('div')
                        .text(param.label);

                    let textareaObj = controlObj.append('textarea')
                        .classed(id + '_textarea_' + param.name, true)
                        .attr('name', id + '_textarea_' + param.name)
                        .text(param.default);

                    self._objs.params[param.name] = textareaObj;
                    // let arrayOfData = Array.from({ length: 101 }, (x, i) => i + 1);
                    // let options = selectObj.selectAll('option')
                    //     .data(arrayOfData)
                    //     .enter()
                    //     .append('option')
                    //     .attr('value', d => d)
                    //     .property('selected',
                    //         d => d === self._core._state.experimentProps[param.name]) // Default
                    //     .text(d => d);
                    
                } else {
                    
                    controlObj.text(param.label + ' ');

                    let selectObj = controlObj.append('select')
                        .classed(id + '_select_' + param.name, true)
                        .attr('name', id + '_select_' + param.name);
                    self._objs.params[param.name] = selectObj;
                    let arrayOfData = Array.from({ length: 101 }, (x, i) => i + 1);
                    let options = selectObj.selectAll('option')
                        .data(arrayOfData)
                        .enter()
                        .append('option')
                        .attr('value', d => d)
                        .property('selected',
                            d => d === self._core._state.experimentProps[param.name]) // Default
                        .text(d => d);

                }





            } else {
                // Update the param value from core
            }
        }


    }

    __generateHtmlElements(id) {
        let self = this;

        let withinElement = self._core._settings.originalProperties.inElement || 'body';

        let container = d3.select(withinElement)
            .append('div')
            .classed(id, true)
            .classed('mdpAI', true)
            .on('dragenter', function(evt) {
                d3.select(this).style('background-color', 'rgba(0,0,255,0.1)');
                d3.event.preventDefault();
            })
            .on('dragover', function(evt) {
                d3.select(this).style('background-color', 'rgba(0,0,255,0.1)');
                d3.event.preventDefault();
            })
            .on('dragleave', function() {
                d3.select(this).style('background-color', 'rgba(0,0,255,0)');
            })
            .on('dragexit', function() {
                d3.select(this).style('background-color', 'rgba(0,0,255,0)');
            })
            .on('drop', function() {
                d3.event.stopPropagation();
                d3.event.preventDefault();

                if (d3.event.dataTransfer.files.length > 0) {
                    console.log(d3.event.dataTransfer.files);
                    self.__loadFiles(d3.event.dataTransfer.files);
                }

            });

        self._objs.container = container;
        self._objs.container.attr('mode', self._state.mode);

        self.__generateHtmlElementsControls(id, container);

        let mdpsContainer = container.select('.sandboxContents').append('div')
            .classed('mdpsContainer', true);

        self._objs.mdpsContainer = mdpsContainer;

        // Render once after creating the initial elements
        self.render()
    }

    __collateGeneratorParametersAndGenerate() {
        let self = this;
        let params = {}

        for (let [propName, obj] of Object.entries(self._objs.params)) {
            let propVal = obj.property('value');
            params[propName] = isNaN(+propVal)?propVal:+propVal;
            // Type actually has both the type and version embedded
            if(propName === 'type') {
                let selectedOptionData = obj.selectAll('option')
                    .filter(d=>d.id === propVal).datum();
                params.type = selectedOptionData.type;
                params.version = selectedOptionData.version;
            }
        }

        // debugger;

        // Additional params
        params['_generation'] = {
            numInstances: self._objs.container.select('.numGeneratorsSelect').property('value')
        }
        // debugger;
        // self._core.updateProperties(params);


        self._core.addMDPviaParams(params);

        // self._objs.map(obj => {
        //     params[obj]
        // })
        // self._core.updateProperties({
        //     states: +self._objs.selectState.property('value')
        // });
    }

    render() {
        let self = this;

        let mdps = self._core._data.mdps;

        // // get an instance ID if not already there
        // for (let mdpData of mdps) {
        //     if(mdpData.instanceID === undefined) {
        //         mdpData.instanceID = self.__generateInstanceID();
        //     }
        // }
        let connectedToPythonInterface = self._core.modules.interfacer._s.connectedToServer;

        

        self._objs.container.select('.pythonInterfaceControls')
            .each(function(){
                if (connectedToPythonInterface) {
                    d3.select(this).classed('connectionStatusConnected', true);
                    d3.select(this).classed('connectionStatusDisconnected', false);
                    d3.select(this).select('.pythonConnectionStatus').text('Connected');
                } else {
                    d3.select(this).classed('connectionStatusDisconnected', true);
                    d3.select(this).classed('connectionStatusConnected', false);
                    d3.select(this).select('.pythonConnectionStatus').html('Disconnected, <a href="https://bitbucket.org.com/ideaOwl/mdp.ai/python/benchmarking" target="_blank">see instructions</a>');
                }
            });

        let container = self._objs.mdpsContainer;



        self._objs.container.selectAll('.tab').classed('selected', d => d.id === self._state.mode 
            || (d.id === 'generate' && self._state.mode === 'evaluate'));

        if (self._state.mode === 'benchmark') {
            // self._objs.container.select('.legendSection').style('display', 'none');
            self._objs.container.select('.evaluateSection').style('display', 'block');
            self._objs.container.select('.generateSection').style('display', 'none');
        } else if (self._state.mode === 'evaluate') {
            // self._objs.container.select('.legendSection').style('display', 'block');
            self._objs.container.select('.evaluateSection').style('display', 'none');
            self._objs.container.select('.generateSection').style('display', 'block');
        } else {
            // self._objs.container.select('.legendSection').style('display', 'none');
            self._objs.container.select('.evaluateSection').style('display', 'none');
            self._objs.container.select('.generateSection').style('display', 'block');
        }


        function pushIfNew(arr, val) {
            let stringifiedArr = arr.map(d=>JSON.stringify(d));
            if(stringifiedArr.indexOf(JSON.stringify(val)) === -1) {
                arr.push(val);
            }
        }

        let mdpInstance = container.selectAll('.mdpInstance')
            .data(mdps, d=>d.instanceID);

        let mdpInstanceEnter = mdpInstance
            .enter()
            .insert('div', ':first-child')
            .attr('class', d=>d.instanceID)
            .classed('mdpInstance', true)


            
            
        let mdpInstanceCardContainer = mdpInstanceEnter
                .append('div')
                .classed('flippableCardContainer', true);
            
        let benchmarksContainer = mdpInstanceEnter.append('div')
            .classed('benchmarksContainer', true)
            .classed('hidden', true);

        let returnChartContainer = benchmarksContainer.append('div')
            .classed('returnChartContainer', true);
        
        let returnChartG = returnChartContainer.append('svg')
            .classed('returnChartSVG', true)
            .append('g')
            .classed('returnChartG', true);
            

        let mdpInstanceFrontEnter = mdpInstanceCardContainer
                .append('div')
                .classed('front', true)
                .classed('noActions', mdpData => !mdpData.generationMatrixes._hasActions)
                // .classed('modeEvaluate', true)
                .classed('cardSide', true);
        let mdpInstanceBackEnter = mdpInstanceCardContainer
                .append('div')
                .classed('back', true)
                .classed('cardSide', true)
                .on('click', function(d){
                    d3.select('.' + d.instanceID).classed('showingInfo', false);
                });

            mdpInstanceFrontEnter.append('svg')
                .attr('width', self._core._settings.originalProperties.visualization.width || '100%')
                .attr('height', self._core._settings.originalProperties.visualization.height)
                .attr('viewBox', '0 0 500 500')
                
            let cardDetails = mdpInstanceFrontEnter.append('div')
                .classed('cardDetails', true);

            cardDetails.append('div')
                .classed('cardOneLinerContainer', true)
                .on('click', d=>console.log(d));


            let cardActionsContainer = cardDetails.append('div')
                .classed('cardActionsContainer', true);

            cardActionsContainer.each(function(d){
                d3.select(this).selectAll('i')
                    .data(['refresh', 'show_chart', 'palette', 'save', 'info', 'delete']) // 'palette', 
                    .enter()
                    .append('i')
                    .attr('class', e => 'icon_' + e)
                    .classed('material-icons', true)
                    .text(e => e)
                    .on('click', e => self.__instanceAction(e, d))
                    .on('mouseover', function(e){
                        // let message = '';
                        // if (e === 'refresh') message = 'Regenerate MDP';
                        // if (e === 'show_chart') message = 'Show Chart';
                        // if (e === 'palette') message = 'Visualize Values';
                        // if (e === 'save') message = 'Save as JSON';
                        // if (e === 'info') message = 'See Parameters for this MDP';
                        // if (e === 'delete') message = 'Delete';
                        // self.__updateTooltipMessages(message);
                        // self.__updateTooltip(d3.event);
                    })
                    .on('mousemove', function () {
                        // self.__updateTooltip(d3.event);
                    })
                    .on('mouseout', d => {
                        // self._objs.container.select('.mdpTooltip').classed('show', false);                        
                    })
            })

        let mdpInstanceEnterAndUpdate = mdpInstance.merge(mdpInstanceEnter);
        
        
        mdpInstanceEnterAndUpdate.each(function(d){
            let generatorInfo = d.generator._core.generators.getSpecifiedGenerator(d.generator._params).info;
            let shortName = generatorInfo.type;
            if (generatorInfo.shortNameWithParams !== undefined) {
                shortName = generatorInfo.shortNameWithParams(d.generator._params);
            }
            d3.select(this).select('.cardOneLinerContainer')
                .text(shortName)

            d3.select(this).select('.back').selectAll('*').remove();
            if (d.generator._params.type !== undefined) {
                d3.select(this).select('.back').append('h4').text('Generator: ' + d.generator._params.type);

                for (let [key, value] of Object.entries(d.generator._params)) {
                    if (['seed', 'visualization', 'inElement', '_generation', 'type'].indexOf(key) === -1) {
                        let row = d3.select(this).select('.back').append('div').classed('row', true);
                        row.append('div').classed('key', true).text(key + ': ');
                        row.append('div').text(value);
                    }
                }

            } else {
                d3.select(this).select('.back').append('h4').text('Generator: ' + 'user-provided-matrices');
                let row = d3.select(this).select('.back').append('div').classed('row', true);
                let paramsWOSeed = JSON.parse(JSON.stringify(d.generator._params));
                delete paramsWOSeed['seed'];
                row.append('textarea')
                    .attr('rows', 9)
                    .classed('monoTextarea', true).text(JSON.stringify(paramsWOSeed), ' ', 2)
                    .on('click', function () { d3.event.stopPropagation(); });
                
            }
        })



        mdpInstance.exit()
            .remove();

        mdpInstance = mdpInstance.merge(mdpInstanceEnter);


        mdpInstance.each(function(mdpData) {
            let meta = mdpData.generationMatrixes._meta;
            let mdpHasActions = mdpData.generationMatrixes._hasActions;
            let mdpInstanceDiv = d3.select(this);
            if(mdpData.vizVars.simulation === undefined) {
                mdpData.vizVars.simulation = null;
            }
            if (mdpData.vizVars.lastGenerationMatrixes === undefined) {
                mdpData.vizVars.lastGenerationMatrixes = JSON.stringify([mdpData.generationMatrixes['state-action-state'], mdpData.generationMatrixes['state-action-state-reward']]);
            }

            let interfacerVars = mdpData.generationMatrixes.interfacerVars;
            let qValuesFromAgentExists = interfacerVars !== undefined
                && interfacerVars._fromAgent !== undefined
                && interfacerVars._fromAgent.maxQValue !== undefined;            

            let svg = d3.select(this).select('svg');
            let svgContent = svg.select('.svgContent');

            if(svgContent.empty()) {
                svgContent = d3.select(this).select('svg')
                .append('g').classed('svgContent', true);
            }

            svg.call(d3.zoom().on("zoom", function () {
               svgContent.attr("transform", d3.event.transform)
            }))



            // let debugBox = svg.select('.debugBox');

            // if(debugBox.empty()) {
            //     debugBox = svg.append('rect')
            //         .classed('debugBox', true)
            //         .attr('x',0)
            //         .attr('y',0)
            //         .attr('width',10)
            //         .attr('height',10)
            //         .attr('fill', 'rgba(0,255,0,0.4)')
            //         .on('click', d => {console.log(mdpData)});
            // }



            // let removeBox = svg.select('.removeBox');

            // if(removeBox.empty()) {
            //     removeBox = svg.append('rect')
            //         .classed('removeBox', true)
            //         .attr('x',490)
            //         .attr('y',0)
            //         .attr('width',10)
            //         .attr('height',10)
            //         .attr('fill', 'rgba(255,0,0,0.4)')
            //         .on('click', d => {
            //             self._core.__removeMDP(d3.select(this).datum().instanceID)
            //             self.render();
            //         });
            // }

            // let saveBox = svg.select('.saveBox');

            // if(saveBox.empty()) {
            //     saveBox = svg.append('rect')
            //         .classed('saveBox', true)
            //         .attr('x',0)
            //         .attr('y',10)
            //         .attr('width',10)
            //         .attr('height',10)
            //         .attr('fill', 'rgba(0,0,255,0.4)')
            //         .on('click', d => {
            //             self.saveMDPsToFile(d);
            //         });
            // }


            if (self._state.mode === 'benchmark' 
                && connectedToPythonInterface
                && interfacerVars !== undefined) {

                let x = d3.scaleLinear()
                    .rangeRound([0, 150]);
                let y = d3.scaleLinear()
                    .rangeRound([150, 0]);

                x.domain([0,interfacerVars.returnHistory.length]);
                y.domain(d3.extent(interfacerVars.returnHistory, d => d));

                mdpInstanceDiv.select('.returnChartG').selectAll('*').remove();
                mdpInstanceDiv.select('.returnChartG')
                    .attr('transform', 'translate(40,10)')

                let xTickValues = [0], yTickValues = [0];
                if (interfacerVars.returnHistory.length > 0) {
                    xTickValues = [0, interfacerVars.returnHistory.length-1]
                    yTickValues = [0, interfacerVars.returnHistory[interfacerVars.returnHistory.length - 1]]


                    mdpInstanceDiv.select('.returnChartG').append("g")
                        .attr("transform", "translate(0," + 150 + ")")
                        .call(d3.axisBottom(x).tickValues(xTickValues))
                        .append('text')
                        .attr("text-anchor", "end")
                        .attr("fill", "#000")
                        .attr('y', 16)
                        .attr('x', 90)
                        .text('# Steps')
                        .select(".domain")
                        .remove();

                    mdpInstanceDiv.select('.returnChartG').append("g")
                        .call(d3.axisLeft(y).tickValues(yTickValues))
                        .append("text")
                        .attr("fill", "#000")
                        .attr("transform", "rotate(-90)")
                        .attr("y", 6)
                        .attr("dy", "0.71em")
                        .attr("text-anchor", "end")
                        .text("Return");                    
                }


                mdpInstanceDiv.select('.returnChartG')
                    .append('path')
                    .datum(interfacerVars.returnHistory, (d,i)=>i)
                    .attr("fill", "none")
                    .attr("stroke", "red")
                    .attr("stroke-linejoin", "round")
                    .attr("stroke-linecap", "round")
                    .attr("stroke-width", 1.5)
                    .attr("d", d3.line()
                        .x(function (d,i) { return x(i); })
                        .y(function (d) { return y(d); })
                    );
            }
            









            mdpData.vizVars.mergedNodes = [];
            mdpData.vizVars.linksData = [];


            // Setup mergenodes
            for (let stateNode of meta) {
                let isTerminal = stateNode.terminal !== undefined && stateNode.terminal;
                pushIfNew(mdpData.vizVars.mergedNodes, stateNode)
                if(isTerminal) continue;
                let hasActions = stateNode.actions;
                if (hasActions) {
                    for (let actionNode of stateNode.actions) {
                        pushIfNew(mdpData.vizVars.mergedNodes, actionNode)
                    }
                }
            }

            if(mdpData.vizVars.fixedNodePositions) {
                mdpData.vizVars.mergedNodes.filter(node=> {
                    node.fx = mdpData.vizVars.fixedNodePositions[node.nodeID].fx;
                    node.fy = mdpData.vizVars.fixedNodePositions[node.nodeID].fy;
                })
            } else {
                mdpData.vizVars.mergedNodes.filter(node=> {
                    node.fx = undefined;
                    node.fy = undefined;
                })
            }


            let dataWhereAgentIs = mdpData.vizVars.mergedNodes.filter(d=>{
                if(d.agent) {
                    return true;
                }
            })[0];

            // mdpData.vizVars.mergedNodes.filter(d=>{
            //     d.fx = 100;
            //     d.fy = 100;
            // })

            let simulation = null;
            if(mdpData.vizVars.simulation) {
                simulation = mdpData.vizVars.simulation;
            } else {
                simulation = d3
                    .forceSimulation();
                mdpData.vizVars.simulation = simulation;
            }


            simulation
                .nodes(mdpData.vizVars.mergedNodes, d=>d.nodeID)
                .force('charge', d3.forceManyBody().strength(-100).distanceMin(10))
                .force('center', d3.forceCenter(500 / 2, 500 / 2))
                // .force('x', d3.forceX(500 / 2).strength(0.5))
                // .force('y',  d3.forceY(500 / 2).strength(0.05))


            // setTimeout(function(){simulation.force('charge', d3.forceManyBody().strength(-100).distanceMin(50))}, 2000)

            //Create the link force
            //We need the id accessor to use named sources and targets
            for (let currNode of mdpData.vizVars.mergedNodes) {
                let isTerminal = currNode.terminal !== undefined && currNode.terminal;
                if (currNode.type === 'state' && !isTerminal) {
                    let hasActions = currNode.actions;
                    if (hasActions) {
                        currNode.actions.map(d=>{
                            pushIfNew(mdpData.vizVars.linksData, {
                                'source': currNode.nodeID,
                                'target': d.nodeID
                            })
                        })
                    } else {
                        // No actions
                        currNode.to.map(d=>{
                            pushIfNew(mdpData.vizVars.linksData, {
                                'source': currNode.nodeID,
                                'target': d.toStateID
                            })
                        })

                    }

                }
                if(currNode.type === 'action') {
                    currNode.to.map(d=>{
                        if(d.prob !== 0) {
                            pushIfNew(mdpData.vizVars.linksData, {
                                'source': currNode.nodeID,
                                'target': d.toStateID
                            })
                        }
                    })
                }
            }

        // mdpInstance.selectAll('svg')
        //
        // let svg = mdpInstance.append('svg')
        //     .attr('width', 500)
        //     .attr('height', 500)
        //     .merge(svg);



        var link_force =  d3.forceLink(mdpData.vizVars.linksData)
                                .id(d=>d.nodeID)
                                .distance(d => {
                                    if(d.source.actions) return 10;
                                    return meta.length*(15)
                                })

        simulation.force("links", link_force);


        let linksContainer = svgContent.select('g.linksContainer');

        if(linksContainer.empty()) {
            linksContainer = svgContent.append("g").classed('linksContainer', true);
        }


        //draw lines for the links
        let linkElements = linksContainer
            .selectAll("line")
            .data(mdpData.vizVars.linksData, d=> {
                return d.source.nodeID + '_to_' + d.target.nodeID;
            });

            linkElements.exit()
                .transition()
                .duration(self._state.mode === 'benchmark' ?  0 : self._state.transitionDuration)
                .attr('stroke-width', 0)
                .remove();

        let linkElementsEnter = linkElements
            .enter().append("line")
            .attr('stroke','rgba(255,255,255,0)')
            .attr('stroke-width', 0)
            .on('mouseover', function (d) {
                let mdpTooltip = self._objs.container.select('.mdpTooltip');
                mdpTooltip.selectAll('*').remove();
                
                if (d.source.stateID !== undefined) {
                    mdpTooltip.append('div').text('Action: ' + d.target.actionID);
                }
                if (d.target.stateID !== undefined) {
                    let targetTransition = d.source.to.filter(a => a.toStateID === d.target.stateID)[0];
                    mdpTooltip.append('div').text('Probablity: ' + Number.parseFloat(targetTransition.prob).toFixed(3));
                    mdpTooltip.append('div').text('Reward: ' + targetTransition.reward);
                }

                self.__updateTooltip(d3.event, 'mdp', mdpInstanceDiv.select('.front.cardSide').classed('modeEvaluate'));
            })
            .on('mouseout', function (d) {
                self._objs.container.select('.mdpTooltip').classed('show', false);
            })
            .on('mousemove', function (d) {
                self.__updateTooltip(d3.event, 'mdp', mdpInstanceDiv.select('.front.cardSide').classed('modeEvaluate'));
            });


        linkElements = linkElements.merge(linkElementsEnter);

        linkElements
            .transition()
            .duration(self._state.mode === 'benchmark' ? 0 : self._state.transitionDuration)
            // .delay((d,i)=>i*50)
            .attr('stroke', d=>{
                // debugger;
                if(self._state.mode === 'generate' 
                    && mdpInstanceDiv.selectAll('.modeEvaluate').empty()) {
                    return 'black';
                }

                if (self._state.mode === 'benchmark') {
                    if (!mdpHasActions) return 'black';
                    if(dataWhereAgentIs) {
                    }
                     let isChildState = dataWhereAgentIs
                        && (d.source.agent === true || dataWhereAgentIs.actions.filter(da => da === d.source).length === 1)
                        && dataWhereAgentIs.actions.filter(action=> {
                         return action.to.filter(toStateMeta => {
                             return d.source.agent
                                && toStateMeta.toStateID === d.target.stateID
                                && toStateMeta.prob > 0
                         }).length > 0;
                     })
                     if (isChildState) {

                     } else {
                         return 'rgba(0,0,0,0.05)';
                     }
                }

                if(mdpData.generationMatrixes._optimalPi) {
                    if(d.target.type === 'action' ) {
                        let stateID = +d.source.stateID;
                        let optimalActionID = +mdpData.generationMatrixes._optimalPi[stateID];
                        if(+d.target.actionID === optimalActionID) {
                            return 'green';
                        }
                        return 'orange';
                    } else {
                        // target is a node
                        if(d.source.to.filter((toStateMeta) =>
                            +toStateMeta.toStateID === +d.target.stateID
                            && toStateMeta.prob > 0
                            && toStateMeta.reward > 0).length > 0) {
                            return 'red';
                        } else {
                            return 'rgba(0,0,255,0.2)';
                        }
                    }
                } else {
                    return 'black';
                }
            })
          .attr("stroke-width", d=>{
              if (self._state.mode === 'generate' && mdpInstanceDiv.selectAll('.modeEvaluate').empty()) {
                  return 2;
              }
              if(self._state.mode === 'benchmark') {
                  if (!mdpHasActions) return 2;

                    return 4;
                // if (d.source.type)
                // console.log(d);
                //   let isChildState = dataWhereAgentIs
                //      && (d.source.agent === true || dataWhereAgentIs.actions.filter(da => da === d.source).length === 1)
                //      && dataWhereAgentIs.actions.filter(action=> {
                //       return action.to.filter(toStateMeta => {
                //           return d.source.agent
                //              && toStateMeta.toStateID === d.target.stateID
                //              && toStateMeta.prob > 0
                //       }).length > 0;
                //   })

                //   if (!isChildState) {
                //       return 2;
                //   }
              }
              if(d.target.type !== 'action') {
                  // target is a node
                  // console.log()
                  let prob = 0;

                  d.source.to.filter(toStateMeta => {
                      if(+toStateMeta.toStateID === +d.target.stateID
                          && toStateMeta.prob > 0) {
                              prob = +toStateMeta.prob*10;
                          }
                  });

                  return prob;
              }
              return 2;
          });


        let nodesContainer = svgContent.select('g.nodesContainer');

        if(nodesContainer.empty()) {
            nodesContainer = svgContent.append("g").classed('nodesContainer', true);
        }


        let nodeElements = nodesContainer
            .selectAll('.nodes')
            .data(mdpData.vizVars.mergedNodes, d=>d.nodeID);

        let nodeElementsEnter = nodeElements
            .enter()
            .append("g")
            .classed("nodes", true)
            .style('cursor', 'pointer')
            .on('click', d=> {
                console.log(d);
            })
            .on('mousemove', function (d) {
                self.__updateTooltip(d3.event, 'mdp', mdpInstanceDiv.select('.front.cardSide').classed('modeEvaluate'));
            })
            .call(d3.drag()
              .on("start", dragstarted)
              .on("drag", dragged)
              .on("end", dragended));




          nodeElementsEnter
              .append('circle')
              .classed('agentCircleBy', true);

        nodeElementsEnter
            .append('circle')
            .classed('stateCircle', true);

        nodeElementsEnter
            .append('text')
            .classed('stateName', true)
            .attr('text-anchor', 'middle')
            .attr('x',0)
            .attr('y',5)
            .text(d => d.stateID);

        let agentCircle = svgContent.select('.agentCircle');

        if(agentCircle.empty()) {
            agentCircle = svgContent.append('circle')
                .classed('agentCircle', true);
        }
        agentCircle
            .attr('r', 16)
            .attr('fill', 'rgba(255,255,0,0.6)')
            .style('opacity', ()=>{
                if (self._state.mode !== 'benchmark') {
                    return 0
                } else {
                    if (!mdpHasActions) return 0;
                    return 1
                }
            })




        nodeElements = nodeElements.merge(nodeElementsEnter);

            nodeElements
                .on('mouseover', function (d) {
                    svgContent.selectAll('line')
                        .filter(function (ld) { return ld.source.nodeID === d.nodeID })
                        .attr('opacity', '1')

                    svgContent.selectAll('line')
                        .filter(function (ld) { return ld.source.nodeID !== d.nodeID })
                        .attr('opacity', '0.1')

                    if (d.actions) {
                        svgContent.selectAll('line')
                            .filter(function (ld) { return d.actions.indexOf(ld.source) > -1 })
                            .attr('opacity', '1')

                        let statesTo = [];
                        d.actions.filter((actionMeta) => {
                            actionMeta.to.filter((toStateMeta) => {
                                if (toStateMeta.prob > 0) {
                                    statesTo.push(+toStateMeta.toStateID);
                                }
                            })
                        })

                        svgContent.selectAll('.nodes')
                            .attr('opacity', '0.1')

                        svgContent.selectAll('.nodes')
                            .filter(function (nd) {
                                if (nd.stateID === undefined) return false;
                                return statesTo.indexOf(+nd.stateID) > -1
                            })
                            .attr('opacity', '1')
                    }

                    // is action node
                    if (d.actionID) {
                        svgContent.selectAll('.nodes')
                            .attr('opacity', '0.1')

                        svgContent.selectAll('line')
                            .filter(function (ld) { return ld.target === d })
                            .attr('opacity', '1')

                        svgContent.selectAll('.nodes')
                            .filter(function (nd) {
                                if (nd.stateID === undefined) return false;
                                let isSource = nd.actions.indexOf(d) > -1;
                                let isTarget = d.to.filter(
                                    (toStateMeta) => toStateMeta.prob > 0 && +toStateMeta.toStateID === +nd.stateID).length > 0;
                                return isSource || isTarget;
                            })
                            .attr('opacity', '1')

                    }

                    let mdpTooltip = self._objs.container.select('.mdpTooltip');
                    mdpTooltip.selectAll('*').remove();
                    if (d.terminal) {
                        mdpTooltip.append('div').text('Terminal State');
                    } else if (d.actionID) {
                        if (self._state.mode === 'benchmark') {
                            if (qValuesFromAgentExists) {
                                mdpTooltip.append('div').text('Agent\'s Q value: ' + Number.parseFloat(interfacerVars._fromAgent['Q'][+d.fromStateID][+d.actionID]).toFixed(3));
                            }
                        }
                        mdpTooltip.append('div').text('Action: ' + d.actionID);
                    } else {
                        mdpTooltip.append('div').text('Value: ' + Number.parseFloat(d.value).toFixed(3));
                        mdpTooltip.append('div').text('State ID: ' + d.stateID);
                    }
                    self.__updateTooltip(d3.event, 'mdp', mdpInstanceDiv.select('.front.cardSide').classed('modeEvaluate'));

                })
                .on('mouseout', d => {
                    svgContent.selectAll('line')
                        .attr('opacity', '1');
                    svgContent.selectAll('.nodes')
                        .attr('opacity', '1')
                    self._objs.container.select('.mdpTooltip').classed('show', false);
                })

        nodeElements
            .style('opacity', d=> {
                if (self._state.mode === 'benchmark') {
                    if (!mdpHasActions) return 1;
                //     return 1
                // } else {
                    if(dataWhereAgentIs) {
                        let currStateIsAgent = d === dataWhereAgentIs;
                        let currStatesAreChildren = dataWhereAgentIs.actions
                        && dataWhereAgentIs.actions.filter(da =>
                            da === d
                            || da.to.filter(toStateMeta =>
                                toStateMeta.toStateID === d.stateID
                                && toStateMeta.prob > 0).length > 0).length > 0;

                        if (currStateIsAgent) {
                            return 1;
                        }

                        if (currStatesAreChildren) {
                            return 0.65;
                        }
                    }

                    if (d.type === 'action') {
                        return 1;
                    }

                    return 0.1;

                }
            })


        nodeElements.select('.stateName')
        .attr('fill', d => {
            if (self._state.mode !== 'benchmark') {
                return 'black'
            } else {
                if (!mdpHasActions) return 'black';
                if (d.agent) {
                    return 'red';
                } else {
                    return 'black'
                }
            }
        })

        // nodeElements.select('.agentCircle')
        //     .attr('fill', d => {
        //         if (self._state.mode !== 'benchmark') {
        //             return 'black'
        //         } else {
        //             if (d.agent) {
        //                 return 'red';
        //             } else {
        //                 return 'black';
        //             }
        //         }
        //     })

        nodeElements.select('.stateCircle')
            .transition()
            .duration(self._state.mode === 'benchmark' ? 0 : self._state.transitionDuration)
            .attr("r", d=> {
                if (self._state.mode !== 'benchmark') {
                    return d.type==='state'? 14: 3
                }

                if (d.type === 'state') return 14;

                if (d.type === 'action') {
                    if (qValuesFromAgentExists) {

                        if (interfacerVars._fromAgent.maxQValue === 0) return 3;
                        
                        return 3 + 7 * (interfacerVars._fromAgent['Q'][+d.fromStateID][+d.actionID]
                            / interfacerVars._fromAgent.maxQValue);

                    } else {
                        return 3;
                    }
                }

            })
            .attr("fill", function(d, i, a){

                if (self._state.mode === 'benchmark' && d.type === 'action') {
                    if (qValuesFromAgentExists) {
                        if (interfacerVars._fromAgent['Q'][+d.fromStateID][+d.actionID] > 0) {
                            return 'red'
                        } else {
                            return 'black'
                        };
                    }
                }

                if (self._state.mode === 'benchmark') {
                    if (!mdpHasActions) return 'white';
                }

                if(d.type === 'action') {
                    return 'black';
                }

                if (self._state.mode === 'generate' && mdpInstanceDiv.selectAll('.modeEvaluate').empty()) {
                    return 'white';
                } else if(self._state.mode === 'benchmark') {
                    if (d.start) return 'green';
                    if (d.end) return 'grey';
                    return 'white';
                }

                let linksToOthers = false;
                for (let link_datum of mdpData.vizVars.linksData) {
                    if(link_datum.source === d && link_datum.target !== d) {
                        linksToOthers = true;
                        break;
                    }
                }
                let maxValue = Math.max(...mdpData.vizVars.mergedNodes.filter(c=>c.value && !c.terminal).map(c=>c.value));

                let colorScale = d3.scaleLinear().domain([0,maxValue]).range(['white', 'red'])
                if(!linksToOthers) {
                    return 'grey';
                } else {
                    if(maxValue === 0 || maxValue === -Infinity) return 'white';
                    return colorScale(d.value);
                }
                
            })
            .attr("stroke", 'black')
            .attr("stroke-width", '2');



        function dragstarted(d) {
          if (!d3.event.active) simulation.alphaTarget(0.3).restart();
          d.fx = d.x;
          d.fy = d.y;
        }

        function dragged(d) {
          d.fx = d3.event.x;
          d.fy = d3.event.y;
        }

        function dragended(d) {
          if (!d3.event.active) simulation.alphaTarget(0);
          d.fx = null;
          d.fy = null;
        }

        nodeElements = nodeElements.merge(nodeElementsEnter);

        simulation.on('tick', () => {

            // if(self._state.freezeSimulation) return;

            nodeElements
                .attr('transform', function (node) { 
                    if (mdpData.generator._params.type === 'random-walk' ) {
                        let states = mdpData.generator._params.states;
                        if (node.type === 'state') {
                            node.x = +node.stateID * 40 + (250 - (states/2 - 0.5) * 40); node.y = 260;
                        }

                    }
                    return 'translate(' + node.x + ',' + node.y + ')' 
                })


            //update link positions
            //simply tells one end of the line to follow one node around
            //and the other end of the line to follow the other node around
            linkElements
                .attr("x1", function(d) { return d.source.x; })
                .attr("y1", function(d) { return d.source.y; })
                .attr("x2", function(d) { return d.target.x; })
                .attr("y2", function(d) { return d.target.y; });

        })






        let isDifferent = mdpData.vizVars.lastGenerationMatrixes !== JSON.stringify([mdpData.generationMatrixes['state-action-state'], mdpData.generationMatrixes['state-action-state-reward'] ]);
            // debugger;
            // console.log(mdpData.vizVars.newlyGenerated)
        if (isDifferent || mdpData.vizVars.newlyGenerated === true) {
            simulation.alpha(1).restart();
            mdpData.vizVars.lastGenerationMatrixes = JSON.stringify([mdpData.generationMatrixes['state-action-state'], mdpData.generationMatrixes['state-action-state-reward']]);
            if (mdpData.vizVars.newlyGenerated === true) {
                delete mdpData.vizVars['newlyGenerated'];
            }
        } else {

            let dataWhereAgentIs = null;

            let nodeWithAgent = nodeElements.each(d => {
                if (d.agent) {
                    dataWhereAgentIs = d;
                    return true;
                }
            });
            // debugger;
            if (dataWhereAgentIs !== null) {
                let agentColor = dataWhereAgentIs.end ? 'rgba(255,0,0,0.6)' : 'rgba(255,255,0,0.6)';
                agentCircle
                    .attr('cx', dataWhereAgentIs.x + 20)
                    .attr('cy', dataWhereAgentIs.y + 20)
                    .attr('fill', agentColor)
            }

        }





        });


    }


    __loadFiles(files) {
        let self = this;

        for (let file of files) {
            let reader = new FileReader();
            reader.onload = function (evt) {
                let data = JSON.parse(evt.target.result);

                let isSet = Array.isArray(data);
                // Set
                if (isSet) {
                    self._core._data.mdps = [];
                    for (let dataSlice of data) {
                        self.__loadMDP(dataSlice);
                    }
                } else {
                    self.__loadMDP(data);
                }
                self.render();
            };

            // for (let file of d3.event.dataTransfer.files) {
            reader.readAsText(file);
            // }
        }
    }

    __updateTooltipMessages(messages) {
        let self = this;

        let mdpTooltip = self._objs.container.select('.mdpTooltip');
        mdpTooltip.selectAll('*').remove();
        if (Array.isArray(messages)) {
            for (let message in messages) {
                mdpTooltip.append('div').text(message);
            }
        } else {
            // String
            mdpTooltip.append('div').text(messages);
        }

    }

    __updateTooltip(event, source, evaluate) {
        let self = this;
        if (source === 'mdp' && !evaluate) {
            return;
        }
        let mdpTooltip = self._objs.container.select('.mdpTooltip');
        let x = event.x - 34;
        let y = event.y - +mdpTooltip.node().getBoundingClientRect().height - 5;
        if (x < 5) x = 5;
        if (y < 5) y = 5;
        mdpTooltip
            .classed('show', true)
            .style("left", (x) + "px")
            .style("top", (y) + "px");
    }

    __applyToAllDoubleClickAction(action) {
        let self = this;
        if (action === 'delete') {
            self._core.__removeAllMDPs()
            self.render();
        }
    }


    __applyToAllAction(action) {
        let self = this;
        if (action === 'save') {
            self.saveMDPsToFile();
        } else if (action === 'delete') {
            // self._core.__removeMDP(mdpData.instanceID)
            // self.render();
        } else if (action === 'refresh') {
            self.__regenerateMDPsWithExistingGenerators();
            self.render();
        } else if (action === 'palette') {
            let isAllShowingEvaluate = true;
            d3.selectAll('.mdpInstance .front.cardSide').each(function () {
                if (!d3.select(this).classed('modeEvaluate')) {
                    isAllShowingEvaluate = false;
                }
            })
            d3.selectAll('.mdpInstance .front.cardSide').classed('modeEvaluate', !isAllShowingEvaluate);
            self.render();
        } else if (action === 'info') {
            let isAllShowingInfo = true;
            d3.selectAll('.mdpInstance').each(function(){
                if (!d3.select(this).classed('showingInfo')) {
                    isAllShowingInfo = false;
                }
            })
            d3.selectAll('.mdpInstance').classed('showingInfo', !isAllShowingInfo);
        } else if (action === 'map') {
            let legendSection = self._objs.container.select('.legendSection');
            legendSection.classed('hidden', !legendSection.classed('hidden'))
        } else if (action === 'show_chart') {
            let isAllShowingChart = true;
            d3.selectAll('.benchmarksContainer').each(function () {
                if (d3.select(this).classed('hidden')) {
                    isAllShowingChart = false;
                }
            })
            d3.selectAll('.benchmarksContainer').classed('hidden', isAllShowingChart);

        } else if (action === 'folder_open') {
            var e = document.createEvent('UIEvents');
            e.initUIEvent('click', true, true, /* ... */);
            self._objs.container.select('input[name="loadFileInput"]').node().dispatchEvent(e);
            // self._objs.container.select('.loadFileInput').dispatch('click');
            // debugger;
            // self._objs.container.select('input[name="loadFileInput"]').dispatch('click');
            // let isAllShowingChart = true;
            // d3.selectAll('.benchmarksContainer').each(function () {
            //     if (d3.select(this).classed('hidden')) {
            //         isAllShowingChart = false;
            //     }
            // })
            // d3.selectAll('.benchmarksContainer').classed('hidden', isAllShowingChart);

        }
    }

    __instanceAction(action, mdpData) {
        let self = this;
        if (action === 'save') {
            self.saveMDPsToFile(mdpData);
        } else if (action === 'delete') {
            self._core.__removeMDP(mdpData.instanceID)
            self.render();
        } else if (action === 'refresh') {
            self._core.__regenerateSpecificGeneratorMDP(mdpData.instanceID);
            self.render();
        } else if (action === 'palette') {
            let frontSide = d3.select('.' + mdpData.instanceID).select('.front.cardSide');
            frontSide.classed('modeEvaluate', !frontSide.classed('modeEvaluate'))
            self.render();
        } else if (action === 'info') {
            console.log(mdpData);
            d3.select('.'+mdpData.instanceID).classed('showingInfo', true);
        } else if (action === 'show_chart') {
            let benchmarkContainer = d3.select('.' + mdpData.instanceID).select('.benchmarksContainer');
            let toggleValue = !benchmarkContainer.classed('hidden');
            benchmarkContainer.classed('hidden', toggleValue);
        }
    }

    __updateMode(mode) {
        let self = this;
        self._state.mode = mode;
        self._objs.container.attr('mode', mode);

        self.render();
    }

    __generateAndRenderMDPs() {
        let self = this;
        self.__collateGeneratorParametersAndGenerate();
        self.render();
    }

    __regenerateMDPsWithExistingGenerators() {
        let self = this;

        // let mdps = self._core._data.mdps;
        self._core.__regenerateGeneratorMDPs();
        self.render();
    }

    __setNodePositions(d) {
        let self = this;
        let meta = d.generationMatrixes._meta;
        let alreadyFixedPosition = false;
        for (let stateMeta of meta) {
            alreadyFixedPosition = stateMeta.fx !== undefined;
            stateMeta.fx = stateMeta.x;
            stateMeta.fy = stateMeta.y;
        }
    }
    __unsetNodePositions(d) {
        let self = this;
        let meta = d.generationMatrixes._meta;
        let alreadyFixedPosition = false;
        if (!alreadyFixedPosition) {
            for (let stateMeta of meta) {
                stateMeta.fx = stateMeta.x;
                stateMeta.fy = stateMeta.y;
            }
        }
    }

    __hasActionsLink(link) {
        let self = this; 
        if (link.source.type === 'state' && link.target.type === 'state') {
            return false;
        }
        return true;
    }

    __loadMDP(data) {
        let self = this;

        self._objs.container.style('background-color', 'rgba(0,0,255,0)');
        delete data.params['_generation'];
        let ActiveGeneratorClass = self._core.generators.getSpecifiedGenerator(data.params).generator;
        let generatorInstance = new ActiveGeneratorClass(self._core);
        generatorInstance.attachGenerator(self._core, data.params);
        self._core.__updateGenerationMatrixes({
            generationMatrixes: data.generationMatrixes,
            params: data.params,
            generator: generatorInstance,
            vizVars: {linksData:[], mergedNodes: []}
        })
    }

    saveMDPsToFile(d) {
        let self = this;
        // Part 1: set node positions
        if(d === undefined) {
            for(let mdpData of self._core._data.mdps) {
                self.__setNodePositions(mdpData);
            }
        } else {
            self.__setNodePositions(d);
        }

        // Part 2: save
        self._core.save(d === undefined ? undefined: d.instanceID);

        // Part 3: unset node positions
        if(d === undefined) {
            for(let mdpData of self._core._data.mdps) {
                self.__unsetNodePositions(mdpData);
            }
        } else {
            self.__unsetNodePositions(d);
        }

    }

    generateViz(core, properties) {
        let self = this;
        let coreAlreadyHasViz = core.modules.visualization !== null;

        if (coreAlreadyHasViz) {
            return;
        }

        let uniqueID = self.__generateInstanceID();
        self._state.id = uniqueID;
        self.__generateHtmlElements(uniqueID);
    }

}


export default MDPAI_Visualization;
