'use strict'
import nj from '../_dependencies/numjs.0.15.1';
import MDP_AI_GENERATOR_v1 from './templates/mdpai_generator_v1.js';

class MDPAI_Generator_USER_PROVIDED_MATRICES_v1 extends MDP_AI_GENERATOR_v1{

    static generatorInfo() {
        return {
            type: 'user-provided-matrices',
            name: 'User Provided Matrices',
            version: 'v1_001',
            parameters: [
                { name: 'matrices', label: 'Matrices', type: 'textarea', default: '{\n    "state-action-state":\n        [\n            [\n                [0, 0, 0, 1],\n                [0, 1, 0, 0]\n            ],\n            [\n                [0, 0, 1, 0],\n                [0.5, 0, 0, 0.5]\n            ],\n            [\n                [0, 0, 0, 1],\n                [0, 0, 1, 0]\n            ],\n            [\n                [0, 0, 1, 0],\n                [0, 0, 1, 0]\n            ]\n        ],\n    "state-action-state-reward":\n        [\n            [\n                [0, 0, 0, 1],\n                [0, 0, 0, 0]\n            ],\n            [\n                [0, 0, 0, 0],\n                [0, 0, 0, 0]\n            ],\n            [\n                [0, 0, 0, 0],\n                [0, 0, 0, 0]\n            ],\n            [\n                [0, 0, 0, 0],\n                [0, 0, 0, 0]\n            ]\n        ],\n    "terminal-states": ["3"]\n}'},
            ]
        }
    }

    generateMatrixes(p) {
        let self = this;
        
        let matrices = JSON.parse(JSON.stringify(p));
        // debugger;
        // if (matrices.matrices !== undefined) {
        //     matrices = JSON.parse(matrices.matrices.replace(/'/g, '"'));
        // }

        // TODO: Add matrices
        let errors = [];

        return {
            'state-action-state': matrices['state-action-state'],
            'state-action-state-reward': matrices['state-action-state-reward'],
            'terminal-states': matrices['terminal-states'],
            'errors': errors
        };

    }


}


export default MDPAI_Generator_USER_PROVIDED_MATRICES_v1;
