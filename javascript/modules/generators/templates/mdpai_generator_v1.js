'use strict'

class MDP_AI_GENERATOR_v1 {

    constructor(core, params) {
        let self = this;
        if (params === undefined) return;
        self.__setupSelfVariables(core, params);
        self.generate(params);
    }

    __setupSelfVariables(core, params) {
        let self = this;
        self._core = core;
        self._params = params;
        self._s = {
            v: {},
            pi: {}
        };
    }

    attachGenerator(core, params) {
        let self = this;
        self.__setupSelfVariables(core, params);
    }

    generate(params) {
        let self = this;
        let generationMatrixes = self.generateMatrixes(params);

        generationMatrixes['errors'] = generationMatrixes['errors'] || [];
        generationMatrixes['_meta'] = generationMatrixes['_meta'] || self.__translateMatricesIntoMeta(generationMatrixes);

        // Todo: Implement start/end state options other than this
        if (generationMatrixes['start-state'] !== undefined) {
            let startState = +generationMatrixes['start-state'];
            generationMatrixes['_meta'][startState]['start'] = true;
            generationMatrixes['_meta'][startState]['agent'] = true;
        } else {
            generationMatrixes['_meta'][1]['start'] = true;
            generationMatrixes['_meta'][1]['agent'] = true;
        }
        generationMatrixes['_meta'][generationMatrixes['_meta'].length - 1]['end'] = true;

        // Update the core
        self._core.__updateGenerationMatrixes({
            generationMatrixes: generationMatrixes,
            generator: self,
            vizVars: { mergedNodes: [], linksData: [], newlyGenerated: true}
        });
    }

    __translateMatricesIntoMeta(generationMatrixes) {

        let meta = [];

        if (generationMatrixes['state-action-state']) {
            let transitionMatrix = generationMatrixes['state-action-state'];

            meta = [];

            for (let [stateID, actionsNextStatesPair] of Object.entries(transitionMatrix)) {

                
                meta[stateID] = {
                    'stateID': stateID,
                    'actions': [],
                    'nodeID': stateID,
                    'type': 'state'
                }
                
                if (generationMatrixes['terminal-states'] 
                    && generationMatrixes['terminal-states'].indexOf(stateID) > -1) {
                    meta[stateID]['terminal'] = true;
                }

                for (let [actionID, nextStates] of Object.entries(actionsNextStatesPair)) {
                    let currAction = {'actionID': actionID, 'type': 'action', 'fromStateID': stateID, 'nodeID': 's'+stateID+'a'+actionID,'to':[]};
                    for (let [nextStateID, prob] of Object.entries(nextStates)) {
                        currAction.to.push({
                            'toStateID': nextStateID,
                            'prob': prob,
                            'reward': generationMatrixes['state-action-state-reward'][stateID][actionID][nextStateID]
                        });
                    }
                    meta[(stateID)].actions.push(currAction);
                }

            }

        }



        if (generationMatrixes['transition']) {

            let transitionMatrix = generationMatrixes['transition'];

            let generatedReward = Array.from(Array(transitionMatrix.length), () => Array.from(Array(transitionMatrix.length), () => Math.random()*1));
            generatedReward = generatedReward.map(d=> d.map(f=>Math.random() < 0.2 ? f : 0));

            meta = [];
            let reward = generationMatrixes['state-state-reward'] || generatedReward;

            for (let [stateFromID, toStates] of Object.entries(transitionMatrix)) {
                meta[+stateFromID] = {
                    'stateID': +stateFromID,
                    'nodeID': +stateFromID,
                    'to': [],
                    'type': 'state'
                }
                toStates.map((prob,i) => {
                    if(prob>0) {
                        meta[+stateFromID].to.push({
                            'toStateID': i,
                            'prob': prob,
                            'reward': reward[stateFromID][i]
                        })
                    }
                })
            }


        }


        return meta;
    }


}

export default MDP_AI_GENERATOR_v1;
