'use strict'
import MDP_AI_GENERATOR_v1 from './templates/mdpai_generator_v1.js';

class MDPAI_Generator_EQUAL_STATES_ONLY_v1 extends MDP_AI_GENERATOR_v1{

    static generatorInfo() {
        return {
            type: 'state-only-equal-transitions',
            name: 'States Only: Equal Everything',
            version: 'v1_001',
            parameters: [
                {name: 'states', label: '# States', type: Number, default: 10, optionalRange: [1,30]},
            ]
        }
    }

    // parameter p contains input from user or dropdown, in this case having
    // only p.states.
    generateMatrixes(p) {
        let self = this;

        let transitionProbability = 1/p.states;
        let transitionMatrix = [];
        // Create an nxn matrix, where n = # of states.
        let stateArrayFrom0 = Array.from(Array(p.states)); // [0 .. p.states]
        for (let stateFrom in stateArrayFrom0) {
            let stateFromTo = [];
            for (let stateTo in stateArrayFrom0) {
                stateFromTo.push(transitionProbability);
            }
            transitionMatrix.push(stateFromTo);
        }

        return {'transition': transitionMatrix}
    }

}

export default MDPAI_Generator_EQUAL_STATES_ONLY_v1;
