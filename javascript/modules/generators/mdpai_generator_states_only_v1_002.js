'use strict'
import nj from '../_dependencies/numjs.0.15.1';
import MDP_AI_GENERATOR_v1 from './templates/mdpai_generator_v1.js';

class MDPAI_Generator_STATES_ONLY_v1_002 extends MDP_AI_GENERATOR_v1{

    static generatorInfo() {
        return {
            type: 'states-only',
            name: 'States Only: Variable Branching Factor',
            version: 'v1_002',
            parameters: [
                {name: 'states', label: '# States', type: Number, default: 10, optionalRange: [1,30]},
                {name: 'bMin', label: 'Branching Factor (min)', type: Number, default: 1, optionalRange: [1,30]},
                {name: 'bMax', label: 'Branching Factor (max)', type: Number, default: 5, optionalRange: [1,30]}
            ]
        }
    }

    generateMatrixes(p) {
        let self = this;

        let transitionProbability = 1/p.states;

        // For each state, set transition probability for
        // "branchingFactor" branches to 1, essentially a random identity matrix
        let randomIdentityMatrix = self.__createRandomIdentity(p);
        // for (let stateFrom in Array.from(Array(p.states))) {
        //     let stateFromTo = [];
        //     for (let stateTo in Array.from(Array(p.states))) {
        //         stateFromTo.push(transitionProbability);
        //     }
        //     transitionMatrix.push(stateFromTo);
        // }


        return {'transition': randomIdentityMatrix}

    }

    __createRandomIdentity(p) {
        let self = this;

        let transitionMatrix = nj.zeros([p.states, p.states]).tolist();

        for (let [stateFromID, stateTransitionList] of Object.entries(transitionMatrix)) {
            // Randomly pick which next states to transition to
            let nextPossibleStates = nj.arange(p.states).tolist();
            let nextStates = [];
            for (let i = 0; i < self.__randIntBetween(p.bMin, p.bMax); i++) {
                nextStates.push(nextPossibleStates.splice(Math.floor(Math.random()*nextPossibleStates.length),1)[0])
            }
            for (let nextState of nextStates) {
                stateTransitionList[nextState] = 1;
            }
        }


        return self.__adjustUnconnectedNetworks(transitionMatrix, p);
    }

    __adjustUnconnectedNetworks(transitionMatrix, p) {
        let self = this;

        // console.log('---------------')
        // console.log('Original', JSON.parse(JSON.stringify(transitionMatrix)));
        let randomStateID = Math.floor(Math.random() * p.states);

        let connectedNetwork = [randomStateID];
        // console.log('Start from stateID', randomStateID);
        // Transitions to other states
        transitionMatrix[randomStateID].map((prob,stateTo) => {
            let foundInNetwork = connectedNetwork.indexOf(stateTo) > -1;
            if(prob && !foundInNetwork) connectedNetwork.push(stateTo);
        })
        // console.log('Transitions to other states', connectedNetwork);
        // Transitions from other states
        transitionMatrix.map((transitionList, stateFrom) => {
            let foundInNetwork = connectedNetwork.indexOf(stateFrom) > -1;
            if(transitionList[randomStateID] && !foundInNetwork) connectedNetwork.push(stateFrom);
        })

        // console.log('Full connected Network', connectedNetwork);

        // Go through all unconnected states, then connect them with existing
        // list of connected states, all the while adding the newly connected
        // state id into the list of connected state ids
        let unconnectedStateIDs = self.__shuffle(nj.arange(p.states).tolist().filter(stateID => connectedNetwork.indexOf(stateID) === -1));
        for (let stateID of unconnectedStateIDs) {
            // reset branching values to 0
            transitionMatrix[stateID] = transitionMatrix[stateID].map(d => 0);
            for (let i = 0; i < self.__randIntBetween(p.bMin, p.bMax); i++) {
                transitionMatrix[stateID][self.__pickRandomlyFromArray(connectedNetwork)] = 1;
                if (i === 0) {
                    connectedNetwork.push(stateID);
                }
            }
            // connectedNetwork.push(stateID);
        }

        for (let stateFrom = 0; stateFrom < p.states; stateFrom++) {
            transitionMatrix[stateFrom] = transitionMatrix[stateFrom].map(d=>Math.random()*d);
            let stateMax = transitionMatrix[stateFrom].reduce((carry, d) => carry + d, 0);
            transitionMatrix[stateFrom] = transitionMatrix[stateFrom].map(d=>d/stateMax);
        }
        // console.log('Updated', JSON.parse(JSON.stringify(transitionMatrix)));
        return transitionMatrix;
    }

    __pickRandomlyFromArray(arr) {
        return arr[Math.floor(Math.random() * arr.length)];
    }

    __shuffle(array) {
        var m = array.length, t, i;

        // While there remain elements to shuffle…
        while (m) {

            // Pick a remaining element…
            i = Math.floor(Math.random() * m--);

            // And swap it with the current element.
            t = array[m];
            array[m] = array[i];
            array[i] = t;
        }

        return array;
    }

    __randIntBetween(min, max) {
        return Math.floor(Math.random()*(max-min+1)+min);
    }

}


export default MDPAI_Generator_STATES_ONLY_v1_002;
