'use strict'
import nj from '../_dependencies/numjs.0.15.1';
import MDP_AI_GENERATOR_v1 from './templates/mdpai_generator_v1.js';

class MDPAI_Generator_RANDOM_WALK_WITH_ACTIONS_v1 extends MDP_AI_GENERATOR_v1{

    static generatorInfo() {
        return {
            type: 'random-walk-with-actions',
            name: 'Random Walk with Actions',
            version: 'v1_001',
            parameters: [
                {name: 'statesWOTerminal', label: '# States', type: Number, default: 10, optionalRange: [1,30]},
            ]
        }
    }

    generateMatrixes(p) {
        let self = this;
        p.actions = 2;
        p.states = +p.statesWOTerminal + 2;
        let transitionMatrix = nj.zeros([p.states,p.actions,p.states]).tolist();
        let rewardMatrix = nj.zeros([p.states,p.actions,p.states]).tolist();

        // For each state-action pair
        for (let stateFrom = 0; stateFrom < p.states; stateFrom++) { //nextActionState of transitionMatrix
            var nextActionState = transitionMatrix[stateFrom];
            for (let action = 0; action < p.actions; action++) { //nextStatesForAction of nextActionState) {
                let nextStatesForAction = nextActionState[action];

                // Randomly pick which next states to transition to for this action
                // and also get a uniform, random number per next state
                let nextPossibleStates = nj.arange(p.states).tolist();
                let nextStates = [], nextStateProb = [];
                // debugger;

                let isTerminalState = (+stateFrom === 0) || (+stateFrom === (p.states-1));
                if (isTerminalState) {
                    // // Terminal states need only 1 action
                    if (action === 0) {
                        nextStates.push(stateFrom);
                        nextStateProb.push(1);
                    }
                } else {
                    // let isStateFromTerminal = +stateFrom === 0 || +stateFrom === (p.states-1);
                    // if (!isStateFromTerminal) {
                    // let sideState = Math.abs(+stateFrom - +stateTo) === 1 || Math.abs(+stateTo - +stateFrom) === 1;
                    // if (sideState) {
                    nextStates.push(stateFrom+(action===0?-1:1));
                    nextStateProb.push(1);
                    // };

                    // }
                }
                // nextStates.push(nextPossibleStates.splice(Math.floor(Math.random()*nextPossibleStates.length),1)[0])
                // nextStateProb.push(Math.random());

                // // Normalize transition probabilities such that sum of all
                // // probabilities = 1
                // let totalProb = nextStateProb.reduce((d, currVal) => d + currVal);
                // nextStateProb = nextStateProb.map(d => d/totalProb);
                let probIndex = 0;

                // Update transition probabilities for each state-action pair
                for (let nextState of nextStates) {
                    nextStatesForAction[nextState] = nextStateProb[probIndex];
                    if(nextStateProb[probIndex] > 0) {
                        rewardMatrix[stateFrom][action][nextState] = (+nextState === p.states - 1) ? 1 : 0; //Math.random()*p.reward
                    }
                    probIndex++;
                }

            }

        }

        let errors = [];

        return {
            'state-action-state': transitionMatrix,
            'state-action-state-reward': rewardMatrix,
            'start-state': String(parseInt(p.states/2)),
            'terminal-states': ['0', String(p.states - 1)],
            'errors': errors
        };

    }


}


export default MDPAI_Generator_RANDOM_WALK_WITH_ACTIONS_v1;
