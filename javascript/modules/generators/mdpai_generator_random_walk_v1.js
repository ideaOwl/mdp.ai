'use strict'
import MDP_AI_GENERATOR_v1 from './templates/mdpai_generator_v1.js';

class MDPAI_Generator_RANDOM_WALK_v1 extends MDP_AI_GENERATOR_v1{

    static generatorInfo() {
        return {
            type: 'random-walk',
            name: 'Random Walk',
            version: 'v1_001',
            parameters: [
                {name: 'states', label: '# States', type: Number, default: 10, optionalRange: [1,30]},
            ]
        }
    }

    // parameter p contains input from user or dropdown, in this case having
    // only p.states.
    generateMatrixes(p) {
        let self = this;

        let transitionProbability = 0.5;
        let transitionMatrix = [];
        // Create an nxn matrix, where n = # of states.
        let stateArrayFrom0 = Array.from(Array(p.states)); // [0 .. p.states]
        for (let stateFrom in stateArrayFrom0) {
            let stateFromTo = [];
            for (let stateTo in stateArrayFrom0) {
                let transition = 0;
                let isTerminalState = (+stateFrom === 0 && +stateTo === 0) || (+stateFrom === (p.states-1) && +stateTo === (p.states-1));
                if (isTerminalState) {
                    transition = 1;
                } else {
                    let isStateFromTerminal = +stateFrom === 0 || +stateFrom === (p.states-1);
                    if (!isStateFromTerminal) {
                        let sideState = Math.abs(+stateFrom - +stateTo) === 1 || Math.abs(+stateTo - +stateFrom) === 1;
                        if (sideState) transition = transitionProbability;
                    }
                }
                stateFromTo.push(transition);
            }
            transitionMatrix.push(stateFromTo);
        }

        return {'transition': transitionMatrix}
    }

}

export default MDPAI_Generator_RANDOM_WALK_v1;
