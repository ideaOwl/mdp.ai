'use strict'
import nj from '../_dependencies/numjs.0.15.1';
import MDP_AI_GENERATOR_v1 from './templates/mdpai_generator_v1.js';

class MDPAI_Generator_RANDOM_MDP_v1 extends MDP_AI_GENERATOR_v1{

    static generatorInfo() {
        return {
            type: 'random-mdp',
            name: 'Random MDP',
            version: 'v1_001',
            parameters: [
                {name: 'states', label: '# States', type: Number, default: 10, optionalRange: [1,30]},
                {name: 'actions', label: '# Actions', type: Number, default: 3, optionalRange: [1,30]},
                {name: 'branchingFactor', label: 'Branching Factor', type: Number, default: 5, optionalRange: [1,30]},
                {name: 'reward', label: 'Reward', type: Number, default: 5, optionalRange: [1,30]}
            ]
        }
    }

    generateMatrixes(p) {
        let self = this;
        let transitionMatrix = nj.zeros([p.states,p.actions,p.states]).tolist();
        let rewardMatrix = nj.zeros([p.states,p.actions,p.states]).tolist();

        // For each state-action pair
        for (let stateFrom = 0; stateFrom < p.states; stateFrom++) { //nextActionState of transitionMatrix
            var nextActionState = transitionMatrix[stateFrom];
            for (let action = 0; action < p.actions; action++) { //nextStatesForAction of nextActionState) {
                let nextStatesForAction = nextActionState[action];

                // Randomly pick which next states to transition to for this action
                // and also get a uniform, random number per next state
                let nextPossibleStates = nj.arange(p.states).tolist();
                let nextStates = [], nextStateProb = [];
                for (let i = 0; i < p.branchingFactor; i++) {
                    nextStates.push(nextPossibleStates.splice(Math.floor(Math.random()*nextPossibleStates.length),1)[0])
                    nextStateProb.push(Math.random());
                }

                // Normalize transition probabilities such that sum of all
                // probabilities = 1
                let totalProb = nextStateProb.reduce((d, currVal) => d + currVal);
                nextStateProb = nextStateProb.map(d => d/totalProb);
                let probIndex = 0;

                // Update transition probabilities for each state-action pair
                for (let nextState of nextStates) {
                    nextStatesForAction[nextState] = nextStateProb[probIndex];
                    if(nextStateProb[probIndex] > 0) {
                        rewardMatrix[stateFrom][action][nextState] = (+nextState === 0) ? 1 : 0; //Math.random()*p.reward
                    }
                    probIndex++;
                }

            }

        }

        let errors = [];
        if (p.states < p.branchingFactor) {
            errors.push("You can't have a larger branching factor than the number of states")
        }

        return {
            'state-action-state': transitionMatrix,
            'state-action-state-reward': rewardMatrix,
            'errors': errors
        };

    }


}


export default MDPAI_Generator_RANDOM_MDP_v1;
