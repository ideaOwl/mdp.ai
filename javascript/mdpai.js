'use strict'

import MDPAI_Visualization from './modules/mdpai_visualization.js';
import MDPAI_Solver from './modules/mdpai_solver.js';
import MDPAI_Generation from './modules/mdpai_generation.js';
import MDPAI_Interfacer from './modules/mdpai_interfacer.js';
import { saveAs } from './modules/_dependencies/FileSaver.1.3.8'; 


class MDPAI {
    constructor(params, opts) {
        let self = this;
        let paramSet = params;

        if (Array.isArray(params)) {
            paramSet = params[0] 
        }
        self.__setupSelfVariables(paramSet, opts);

        // Init
        self.generators = new MDPAI_Generation(self, paramSet);

        if (Array.isArray(params) && params.length > 1) {
            for (let i=1;i < params.length; i++) {
                self.addMDPviaParams(params[i]);
            }
        }
        self.solve();

        // Will listen for possible socketio connections
        self.__createInterfacer();
    }


    __setupSelfVariables(params, opts) {
        let self = this;
        if (opts !== undefined) {
            self._settings = opts;
        } else {
            self._settings = {};
        }
        self._settings['originalProperties'] = params

        self._state = {
            hasVisualization: false,
            experimentProps: params,
            hashOfUniqueIDs: {}
        }
        self._data = {mdps:[]};
        self.modules = {
            visualization: null,
            solver: null,
            interfacer: null
        };
    }

    addMDPviaParams(params) {
        let self = this;

        if (params.type === 'user-provided-matrices') {
            params = JSON.parse(params.matrices.replace(/'/g, '"'));
        }
        self.generators.generate(params);
        self.solve();
    }

    updateProperties(params) {
        let self = this;

        for (const [k, v] of Object.entries(params)) {
            self._state.experimentProps[k] = v;
        }

        self.generators.generate(self._state.experimentProps);

        let generationMatrixes = self._data.generationMatrixes[0];
        if (params['solve'] === undefined || params['solve']) {
            let {v, pi} = self.modules.solver.solve(params);
            generationMatrixes['_value'] = v;
            generationMatrixes['_optimalPi'] = pi;
            generationMatrixes['_value'].map((d,i)=>{generationMatrixes['_meta'][i].value = d;});
        }
        
    }

    __updateGenerationMatrixes(matrixData) {
        let self = this;

        let existingInstantiatedGenerator = self._data.mdps.filter(d=>matrixData.generator !== undefined && d.generator === matrixData.generator);

        if (existingInstantiatedGenerator.length > 0) {
            // console.log(existingInstantiatedGenerator[0].generationMatrixes);
            // console.log(matrixData.generationMatrixes);

            existingInstantiatedGenerator[0].generationMatrixes = matrixData.generationMatrixes;
        } else {
            let instanceID = self.__generateInstanceID();
            matrixData.instanceID = instanceID;
            self._data.mdps.push(matrixData)
        }
        self.solve();

        if (self.modules.interfacer && self.modules.interfacer._s.connectedToServer) {
            self.modules.interfacer.__syncMDPsToServer();
        }

    }

    __removeAllMDPs() {
        let self = this;

        self._data.mdps.length = 0;
        if (self.modules.interfacer && self.modules.interfacer._s.connectedToServer) {
            self.modules.interfacer.__syncMDPsToServer();
        }

    }

    __removeMDP(instanceID) {
        let self = this;

        let index = -1;
        self._data.mdps.filter((mdpData,i) => {
            if(mdpData.instanceID === instanceID) {
                index = i;
            }
        })

        self._data.mdps.splice(index, 1);

        if (self.modules.interfacer && self.modules.interfacer._s.connectedToServer) {
            self.modules.interfacer.__syncMDPsToServer();
        }
    }

    __fillProperties(params) {
        return params;
    }

    __createVisualization(params) {
        let self = this;
        self._state.hasVisualization = true;
        self.modules.visualization = new MDPAI_Visualization(self, params);
    }

    __createSolver() {
        let self = this;
        self._state.hasSolver = true;
        self.modules.solver = new MDPAI_Solver(self);
    }

    __createInterfacer() {
        let self = this;
        self._state.hasInterfacer = true;
        self.modules.interfacer = new MDPAI_Interfacer(self);
    }

    getData() {
        let self = this;
        return self._data;
    }

    showProperties() {
        console.log(this.params);
    }

    solve() {
        let self = this;
        if (!self._state.hasSolver) {
            self.__createSolver();
        }
        self.__attemptSolve();
        // console.log(self._data.mdps);
    }

    __regenerateGeneratorMDPs() {
        let self = this;
        let mdps = self._data.mdps;

        for(let mdpData of mdps) {
            mdpData.generator.generate(mdpData.generator._params);
            // console.log(mdpData);
            mdpData.vizVars['newlyGenerated'] = true;
        }
        
    }

    __regenerateSpecificGeneratorMDP(instanceID) {
        let self = this;
        let mdps = self._data.mdps;

        for (let mdpData of mdps) {
            if (mdpData.instanceID === instanceID) {
                mdpData.generator.generate(mdpData.generator._params);
                mdpData.vizVars['newlyGenerated'] = true;
                break;
            }
        }

    }

    __attemptSolve() {
        let self = this;
        for (let mdpData of self._data.mdps) {
            let p = mdpData.generator._params;
            if (true || p['solve'] === undefined || p['solve']) { //
                let {v, pi, hasActions} = self.modules.solver.solve(p, mdpData.generationMatrixes['_meta']);
                mdpData.generationMatrixes['_value'] = v;
                mdpData.generationMatrixes['_hasActions'] = hasActions;
                mdpData.generationMatrixes['_optimalPi'] = pi;
                mdpData.generationMatrixes['_value'].map((d,i)=>{mdpData.generationMatrixes['_meta'][i].value = d;});
            }
        }
    }

    save(mdpID) {
        let self = this;
        let mdps = self._data.mdps;
        let dataToSave = null;
        let saveSet = mdpID === undefined;

        if(!saveSet) {
            let mdpData = mdps.filter(d=>d.instanceID === mdpID)[0];

            dataToSave = {
                'generationMatrixes': mdpData.generationMatrixes,
                'params': mdpData.generator._params
            };
        } else {
            dataToSave = [];
            for (let mdpData of mdps) {
                dataToSave.push({
                    'generationMatrixes': mdpData.generationMatrixes,
                    'params': mdpData.generator._params
                });
            }
        }

        let blob = new Blob([JSON.stringify(dataToSave, null, 2)], {type: "application/json"});

        saveAs(blob, (saveSet?'set':'mdp') + '-mdpai.json');

    }

    render(params) {
        let self = this;
        if (!self._state.hasVisualization) {
            self.__createVisualization(params);
        } else {
            self.modules.visualization.render();
        }
    }

    __generateRandomID(length) {
        return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
    }

    __generateInstanceID() {
        let self = this;
        let idIsUnique = false;
        let currID = "";
        while(!idIsUnique) {
            currID = 'MDPAI_' + self.__generateRandomID(5);
            idIsUnique = self._state.hashOfUniqueIDs[currID] === undefined;
        }
        self._state.hashOfUniqueIDs[currID] = false;
        return currID;
    }
};

export default MDPAI;