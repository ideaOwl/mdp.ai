import resolve from 'rollup-plugin-node-resolve';
import babel from 'rollup-plugin-babel';
import {uglify} from 'rollup-plugin-uglify';
import commonjs from 'rollup-plugin-commonjs';

export default {
  input: 'javascript/mdpai.js',
  output: {
    file: 'bin/mdpai.js',
    format: 'cjs'
  },
  plugins: [
    resolve('.js'),
	commonjs({
		// Explore this as a means to get rolljs to use node_modules
		// namedExports: { './module.js': ['foo', 'bar' ] }, 
	}),
    babel({
      exclude: 'node_modules/**' // only transpile our source code
    }), 
	uglify()
  ]
};
