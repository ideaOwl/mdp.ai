# mdp.ai 

**Note: This is very much an early work-in-progress project, thank you for your patience on the lack of documentation!**

## Important Links

Curious about what this repository is about?  Check out the overview and the playground: 

* [Tool overview](http://mdp.ai/)
* [The Playground](http://mdp.ai/playground)



## Quickstart

You'll need NPM for this codebase.  To build a local version of the tool:

```
npm install
npm run build
```

I just transferred the code from another repository and made a number of simplifications and improvements, including a more automated build process using Webpack.  Unfortunately, it doesn't quite work perfectly yet, it's the next thing on my to do list.